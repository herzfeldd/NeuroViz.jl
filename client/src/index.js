import React from 'react';
import ReactDOM from 'react-dom';

import {StatusContext} from './context.js';
import './index.css';

import '@annotationhub/react-golden-layout/dist/css/goldenlayout-base.css';
import '@annotationhub/react-golden-layout/dist/css/themes/goldenlayout-dark-theme.css';
import { GoldenLayoutComponent } from '@annotationhub/react-golden-layout';
import './golden-layout-override.css';

import { new_session, get_session, delete_session, set_neuron_file, save_session, set_recording_file, set_server_recording_file, get_recording_file, get_session_notes  } from './services/session.js';
import { get_neurons, get_neuron_acg, get_neuron_acg_vs_rate, get_neuron_isi_distribution, get_neuron_rate_vs_time, get_neuron_ccg, realign, findall, delete_neuron, get_neuron_merged_acg, merge_neurons, split_purkinje_cell, get_neuron_template, set_neuron_attributes, convert_neuron, get_neuron_spiketimes, get_neuron_similarity_matrix, get_template_projection, modify_neuron } from './services/neuron.js';
import { get_voltage_across_channels } from './services/voltage.js';
import { get_task } from './services/events.js'
import { RecordingNotes } from './components/notes/notes.js';
import { NeuronList } from './components/neuron_list/neuron_list.js';
import { NeuronSimilarityList } from './components/similarity_list/similarity_list.js';
import { NeuroVizMenuBar } from './components/menu/menu.js';
import { ACG, response_to_acg_data } from './components/line_plots/acg.js';
import { ACGVsRate, has_acg_vs_rate, response_to_acg_vs_rate_data } from './components/line_plots/acg_vs_rate.js';
import { ISI, response_to_isi_data } from './components/line_plots/isi.js';
import { RateTime, response_to_rate_vs_time_data } from './components/line_plots/rate.js';
import { CCG, response_to_ccg_data } from './components/line_plots/ccg.js';
import { MergedACG, has_merged_acg } from './components/line_plots/merged_acg.js';
import { Template, response_to_template_data } from './components/line_plots/template.js';
import { TemplateProjection, response_to_template_projection_data, has_pca_template_projection, has_umap_template_projection } from './components/line_plots/template_projection.js';
import { VoltagePlot, response_to_voltage_data } from './components/line_plots/voltage.js';
import { NeuroVizFooter } from './components/footer/footer.js';
import { Modal } from './components/modal/modal.js';
import { FlatRecordingDialog } from './components/flat_recording_dialog/flat_recording_dialog.js';
import { ProgressBar, IndefiniteProgressBar } from './components/progressbar/progressbar.js';

// Global variable asking whether onWheel is supported
var supports_wheel = false;

function EffectHandler(props) {
  React.useEffect(() => {
    window.addEventListener('beforeunload', props.unloadHandler);
    return () => { window.removeEventListener('beforeunload', props.unloadHandler); };
  }, []);
  React.useEffect(() => {
    props.loadHandler();
  }, []);

  return (null);
}


class App extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      sessionId: null,
      neurons: [], // TODO
      primary_selected_id: null,
      secondary_selected_id: null,
      acg: null, // TODO
      acg_vs_rate: null,
      rate: null,
      isi: null,
      ccg: null,
      notes: null,
      merged_acg: null,
      pca_template_projection: null,
      umap_template_projection: null,
      template: null,
      footer_text: null,
      num_channels: 0,
      progress: 0,
      modal: {visible: false, body: null, header: null, footer: null },
      neuron_filename: null,
      recording_filename: null,
      voltage: null,
      voltage_percent: 0,
      voltage_limits: null,
      primary_spiketimes: [],
      secondary_spiketimes: [],
      mark_list: [],
      similarity_scores: [],
      voltage_num_rows: 4,
    }

    this.handle_file_change = this.handle_file_change.bind(this);
    this.handle_key = this.handle_key.bind(this);
    this.refresh_state = this.refresh_state.bind(this);
    this.clear_selection = this.clear_selection.bind(this);
    this.clear_state = this.clear_state.bind(this);
    this.delete_similarity_matrix_row_column = this.delete_similarity_matrix_row_column.bind(this);
    this.handle_merge = this.handle_merge.bind(this);
    this.handle_pc_split = this.handle_pc_split.bind(this);
    this.handle_save = this.handle_save.bind(this);
    this.set_layout_manager = this.set_layout_manager.bind(this)
    this.handle_drop = this.handle_drop.bind(this)
    this.layout_manager = null;
    this.sorted_hash = null;
    this.add_view = this.add_view.bind(this);
    this.sort_updated = this.sort_updated.bind(this);
    this.convert_neuron_type = this.convert_neuron_type.bind(this);
    this.delete_cached_neuron = this.delete_cached_neuron.bind(this);
    this.handle_local_recording_file_change = this.handle_local_recording_file_change.bind(this);
    this.handle_server_recording_file_change = this.handle_server_recording_file_change.bind(this);
    this.handle_voltage_percent_change = this.handle_voltage_percent_change.bind(this);
    this.modify_spikes_from_neuron = this.modify_spikes_from_neuron.bind(this);
    this.poll_interval = null;
    this.check_alive_interval = null;
    this.check_connection_status = this.check_connection_status.bind(this);
    this.connection_error = false;
    this.flat_recording_dialog = React.createRef();
    this.recording_duration = 0;
    this.voltage_times = [0, 1.0]
    this.acg_cache = {}; // For all caches, the key is neuron hash, value is data from acg call
    this.template_cache = {};
    this.isi_cache = {};
    this.rate_cache = {};
    this.ccg_cache = {};
  }

  handle_unload = (event) => {
    delete_session();
  };
  handle_load = (event) => {
    this.clear_state();
    this.setState({recording_filename: null, neuron_filename: null, footer_text: "", voltage: null, voltage_percent: 0})
    new_session(() => {
        this.setState({modal: {visible: false}});
        //this.check_alive_interval = setInterval(this.check_connection_status, 5 * 1000);
      },
        () => {
      this.setState({modal: {visible: true, header: 'Connection Error', body: 'Unable to connect to server', onClose: this.handle_load } })
      }
    );
  };
  //
  // event_listener = (e) => {
  //   console.log(e)
  //   if (e.data === "recording-file") {
  //     /* Fetch the results */
  //     get_task(e.lastEventId, (response) => this.get_recording_file_response(response))
  //   }
  // }

  cache_acg = (hash, neuron_number=0) => {
    if (hash in this.acg_cache) {
      this.setState({ acg : response_to_acg_data(this.acg_cache[hash], neuron_number, neuron_number === 0 ? null : this.state.acg) });
    } else {
      get_neuron_acg(hash, ((hash, data) => { this.setState({ acg : response_to_acg_data(data, neuron_number, neuron_number === 0 ? null : this.state.acg) }); this.acg_cache[hash] = data; }) );
    }
  }

  cache_neuron_template = (hash, neuron_number=0) => {
    if (hash in this.template_cache) {
      this.setState({ template : response_to_template_data(this.template_cache[hash], neuron_number, neuron_number === 0 ? null : this.state.template) });
    } else {
      get_neuron_template(hash, ((hash, data) => { this.setState({ template : response_to_template_data(data, neuron_number, neuron_number === 0 ? null : this.state.template) }); this.template_cache[hash] = data; }) );
    }
  }

  cache_isi_distribution = (hash) => {
    if (hash in this.isi_cache) {
      this.setState({ isi : response_to_isi_data(this.isi_cache[hash]) });
    } else {
      get_neuron_isi_distribution(hash, ((hash, data) => { this.setState({ isi : response_to_isi_data(data) }); this.isi_cache[hash] = data; }) );
    }
  }

  cache_neuron_rate = (hash, neuron_number=0) => {
    if (hash in this.rate_cache) {
      this.setState({ rate : response_to_rate_vs_time_data(this.rate_cache[hash], neuron_number, neuron_number === 0 ? null : this.state.rate) });
    } else {
      get_neuron_rate_vs_time(hash, ((hash, data) => { this.setState({ rate : response_to_rate_vs_time_data(data, neuron_number, neuron_number === 0 ? null : this.state.rate) }); this.rate_cache[hash] = data; }) );
    }
  }

  cache_ccg = (hash_primary, hash_secondary) => {
    if ((hash_primary in this.ccg_cache) && (hash_secondary in this.ccg_cache[hash_primary])) {
      this.setState({ ccg : response_to_ccg_data(this.ccg_cache[hash_primary][hash_secondary]) });
    } else {
      if (! (hash_primary in this.ccg_cache)) {
        this.ccg_cache[hash_primary] = {}
      }
      get_neuron_ccg(hash_primary, hash_secondary, ((hash1, hash2, data) => { this.setState({ ccg : response_to_ccg_data(data) }); this.ccg_cache[hash_primary][hash_secondary] = data }) );
    }
  }

  change_primary_neuron = (hash) => {
    this.setState({'secondary_selected_id': null, ccg: null, merged_acg: null, secondary_spiketimes: []})
    if (this.state.primary_selected_id === hash) {
      return; /* Do nothing, this is already the selected id */
    }
    this.setState({'primary_selected_id': hash})
    this.cache_acg(hash);
    this.cache_neuron_template(hash);
    this.cache_isi_distribution(hash);
    this.cache_neuron_rate(hash);
    if (has_acg_vs_rate()) {
      get_neuron_acg_vs_rate(hash, (hash, data) => { this.setState({ acg_vs_rate : response_to_acg_vs_rate_data(data) }); console.log(this.state.acg_vs_rate); } );
    }
    if (has_pca_template_projection()) {
    	get_template_projection(hash, () => this.setState({pca_template_projection: "waiting"}), (hash, data) => { this.setState({pca_template_projection: response_to_template_projection_data(data) });}, "pca", null)
    }
    if (has_umap_template_projection()) {
      get_template_projection(hash, () => this.setState({umap_template_projection: "waiting"}), (hash, data) => { this.setState({umap_template_projection: response_to_template_projection_data(data) });}, "umap", null)
    }
    /* Get the spike times for this neuron */
    get_neuron_spiketimes(hash, (hash, data) => { this.setState({primary_spiketimes: data.spiketimes}) }, this.voltage_times[0], this.voltage_times[1])
  }

  change_secondary_neuron = (hash) => {
    if (this.state.secondary_selected_id === hash) {
      return; /* Do nothing, this is already the selected id */
    }
    this.setState({'secondary_selected_id': hash})
    this.cache_acg(hash, 1);
    this.cache_neuron_rate(hash, 1);
    this.cache_neuron_template(hash, 1);
    this.cache_ccg(this.state.primary_selected_id, hash)

    if (has_merged_acg()) {
      get_neuron_merged_acg(this.state.primary_selected_id, hash, ((hash1, hash2, data) => { this.setState({ merged_acg : response_to_acg_data(data) }) }) );
    }
    if (has_pca_template_projection()) {
    	get_template_projection(this.state.primary_selected_id, () => this.setState({pca_template_projection: "waiting"}), (hash, data) => { this.setState({pca_template_projection: response_to_template_projection_data(data) });}, "pca", hash)
    }
    /* Get the spike times for this neuron */
    get_neuron_spiketimes(hash, (hash, data) => { this.setState({secondary_spiketimes: data.spiketimes}) }, this.voltage_times[0], this.voltage_times[1])
  }

  // Called when a new neuron is clicked
  handle_neuron_click = (e, hash, index) => {
    if ((e.ctrlKey || e.metaKey) && this.state.primary_selected_id === null) {
      return; // No primary unit selected, so just return
    }
    if (e.ctrlKey || e.metaKey) {
      this.change_secondary_neuron(hash);
    } else {
      this.change_primary_neuron(hash);
    }
  }

  handle_similarity_click = (e, hash, index) => {
    this.change_secondary_neuron(hash);
  }

  handle_neuron_right_click = (e, hash, index) => {
    e.preventDefault();
  }

  handle_file_change(files, append=false) {
    if (files.length !== 1) {
      return;
    }
    this.setState({modal: {visible: true, header: 'Uploading neuron file...', body: () => {return <ProgressBar value={this.state.progress}/>}, onClose: () => {this.setState({modal: {visible: false}})}}})
    set_neuron_file(files[0], files[0].name, () => {
      this.setState({modal: {visible: false}});
      let mark_list = this.state.mark_list.slice()
      if (! append) {
        mark_list = []
      }
      this.refresh_state()
      this.setState({neuron_filename: files[0].name, mark_list: mark_list})
      this.setState({footer_text: "Recording file: " + this.state.recording_filename + " (" + this.state.neuron_filename + ")"})
    }, (e) => {this.setState({modal: {visible: true, header: "Upload failed", body: "Upload failed with error: " + e + ". Response from server: " + (e.response !== undefined ? e.response.data : "nothing") + ".", onClose: () => {this.setState({modal: {visible: false}}) }}})},
     (e) => { this.setState({progress: e.loaded * 100 / e.total})}, append);
  }

  get_recording_file_response = (response) => {
    console.log(response)
    this.setState({footer_text: "Recording file: " + this.state.recording_filename, modal: {visible: false}, num_channels: response.num_channels})
    this.setState({voltage: null, voltage_percent: 0, voltage_limits: null})
    if (response.num_samples.length > 0) {
      this.recording_duration = response.num_samples[0] / response.sampling_rate[0]
      this.handle_voltage_percent_change(0)
    }
    get_session_notes((data) => {this.setState({'notes': data.notes})});
  }


  handle_local_recording_file_change(files, type="pl2", num_channels=undefined, sampling_rate=undefined, interleaved=undefined, mmap=undefined) {
    if (files.length !== 1) {
      return;
    }
    this.clear_state()
    this.setState({modal: {visible: true, header: 'Uploading recording file...', body: () => {return <ProgressBar value={this.state.progress}/>}, onClose: () => {this.setState({modal: {visible: false}})}}})
      set_recording_file(files[0], files[0].name, () => {
        this.setState({recording_filename: files[0].name})
        this.setState({neuron_filename: null})
        this.setState({notes: null})
        this.setState({modal: {visible: true, header: 'Processing recording...', body: () => {return <IndefiniteProgressBar/>}}});
      }, this.get_recording_file_response,
        (e) => {this.setState({modal: {visible: true, header: "Upload failed", body: "Upload failed with error: " + e + ". Response from server: " + (e.response !== undefined ? e.response.data : "nothing") + ".", onClose: () => {this.setState({modal: {visible: false}}) }}})},
     (e) => { this.setState({progress: e.loaded * 100 / e.total})},
      type,
      num_channels,
      sampling_rate, interleaved, mmap);
  }

  handle_server_recording_file_change(path, type="pl2", num_channels=undefined, sampling_rate=undefined, interleaved=undefined, mmap=undefined) {
    this.clear_state()
    set_server_recording_file(path, () => {
      this.setState({recording_filename: path})
      this.setState({neuron_filename: null})
      this.setState({notes: null})
      this.setState({modal: {visible: true, header: 'Processing recording...', body: () => {return <IndefiniteProgressBar/>}}});
    },
    this.get_recording_file_response,
    (e) => {this.setState({modal: {visible: true, header: "Set file failed", body: "Set server file failed with error: " + e + ". Response from server: " + (e.response !== undefined ? e.response.data : "nothing") + ".", onClose: () => {this.setState({modal: {visible: false}}) }}})},
    type,
    num_channels,
    sampling_rate, interleaved, mmap);
  }

  handle_drag_over(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  /* Handles drop events */
  handle_drop(e) {
    e.stopPropagation();
    e.preventDefault()
    if (e.dataTransfer.files.length === 0) {
      return;
    }
    const filename = e.dataTransfer.files[0].name
    const extension = filename.split('.').pop();
    if (extension === 'pl2') {
      this.handle_local_recording_file_change(e.dataTransfer.files, "pl2")
    } else if (extension === "bin" || extension === "dat") {
      this.flat_recording_dialog.current.set_server(false);
      this.flat_recording_dialog.current.set_callback(this.handle_local_recording_file_change);
      this.flat_recording_dialog.current.set_pl2(false);
      this.flat_recording_dialog.current.set_visible(true, e.dataTransfer.files)
    } else if (extension === "jld2" || extension === "pkl" || extension === "mat") {
      this.handle_file_change(e.dataTransfer.files, (e.ctrlKey || e.metaKey)) // Append if ctrl or meta key pressed
    }
  }



 /* Update the voltage traces based on a percentage of the recording. Width is in seconds */
 handle_voltage_percent_change(percent=0, width=Math.max(0.100, this.voltage_times[1] - this.voltage_times[0])) {
   if (this.state.recording_filename === null || this.recording_duration === 0 || this.percent < 0 || this.percent > 100) {
     return;
   }
   let start_time = (this.recording_duration * percent / 100.0)
   start_time = Math.min(this.recording_duration - width, start_time)
   start_time = Math.max(0, start_time)

   get_voltage_across_channels((response) => {
     this.setState({voltage: response_to_voltage_data(response), voltage_percent: percent});
     this.voltage_times = [start_time, start_time + width]
     if (this.state.primary_selected_id !== null) {
       get_neuron_spiketimes(this.state.primary_selected_id, (hash, data) => { this.setState({primary_spiketimes: data.spiketimes}) }, this.voltage_times[0], this.voltage_times[1])
     }
     if (this.state.secondary_selected_id !== null) {
       get_neuron_spiketimes(this.state.secondary_selected_id, (hash, data) => { this.setState({secondary_spiketimes: data.spiketimes}) }, this.voltage_times[0], this.voltage_times[1])
     }
   }, start_time, start_time + width)
 }

  handle_save(file_type) {
    if (this.state.neuron_filename !== null) {
      save_session(this.state.neuron_filename, file_type)
    }
  }

  clear_selection() {
    this.setState({acg: null, acg_vs_rate: null, ccg: null, isi: null, rate: null, template_cache: null, umap_template_projection: [], pca_template_projection: [], primary_selected_id: null, secondary_selected_id: null, merged_acg: null, template: null, primary_spiketimes: [], secondary_spiketimes: []});
    this.sorted_hash = null;
  }
  clear_state() {
    this.clear_selection()
    this.acg_cache = {}; // For all caches, the key is neuron hash, value is data from acg call
    this.template_cache = {};
    this.isi_cache = {};
    this.rate_cache = {};
    this.ccg_cache = {};
    this.setState({neurons: [], mark_list: [], similarity_scores: []});
    this.neurons = [];
  }
  refresh_state() {
    this.clear_state();
    get_neurons((data) => { this.setState({neurons: data.neurons}); this.neurons = data.neurons; });
    get_neuron_similarity_matrix((data) => { this.setState({similarity_scores: data.values}); });
  }

  // Delete a row and a column from the similarity_scores matrix (list of lists)
  delete_similarity_matrix_row_column(index) {
    if (index > this.state.similarity_scores.length) {
      return; // Do nothing
    }
    let similarity_scores_copy = this.state.similarity_scores.slice()
    similarity_scores_copy.splice(index, 1);
    for (var i = 0; i < similarity_scores_copy.length; i++) {
      similarity_scores_copy[i].splice(index, 1);
    }
    this.setState({similarity_scores: similarity_scores_copy});
  }

  find_neuron_index = (hash, list=undefined) => {
    if (list === undefined) {
      list = this.state.neurons
    }
    let index = null;
    for (var i = 0; i < list.length; i++) {
      if (hash === list[i] || hash === list[i].id) {
        index = i;
        break;
      }
    }
    return index;
  }

  sort_updated(hash_list) {
    this.sorted_hash = hash_list;
  }

  convert_neuron_type(type) {
    if (! this.state.primary_selected_id) {
      return;
    }
    convert_neuron(this.state.primary_selected_id, (response) => {
      if (this.state.primary_selected_id === response) {
        let new_neurons = JSON.parse(JSON.stringify(this.state.neurons));
        let index = this.find_neuron_index(this.state.primary_selected_id)
        let class_type = "Neuron"
        if (type === "pc") {
          class_type = "PurkinjeCell"
        } else if (type === "cs") {
          class_type = "ComplexSpikes"
        }
        new_neurons[index]["class_type"] = class_type
        this.setState({neurons: new_neurons})
      } else {
        this.refresh_state();
        const selection_id = response.id;
        this.change_primary_neuron(selection_id);
      }
    }, type);
  }

  label_neuron = (label) => {
    if (this.state.primary_selected_id  === null || this.state.primary_selected_id === undefined) {
      return
    }
    let index = this.find_neuron_index(this.state.primary_selected_id)
    let layer = this.state.neurons[index]["layer"]
    if (label === "putative_pc" || label === "putative_basket" || label === "pc") {
	layer = "pc_layer"
    } else if (label === "putative_golgi" || label === "putative_mf" || label === "putative_granule" || label === "putative_ubc") {
        layer = "granule_layer"
    } else if (label === "putative_stellate") {
        layer = "molecular_layer"
    }
    let attributes = {label: label, layer: layer}
    if (attributes["layer"].toLowerCase() === "unknown") {
	delete attributes["layer"]
    }
    set_neuron_attributes(this.state.primary_selected_id, attributes, // Set the label attribute
      (response) => {
        let new_neurons = JSON.parse(JSON.stringify(this.state.neurons));
        let index = this.find_neuron_index(this.state.primary_selected_id);
        new_neurons[index]["label"] = label;
        new_neurons[index]["layer"] = layer;
        this.setState({neurons: new_neurons})
      }
    )
  }

 set_neuron_layer = (layer) => {
    if (this.state.primary_selected_id  === null || this.state.primary_selected_id === undefined) {
      return
    }
    set_neuron_attributes(this.state.primary_selected_id, {layer: layer}, // Set the layer attribute
      (response) => {
        let new_neurons = JSON.parse(JSON.stringify(this.state.neurons));
        let index = this.find_neuron_index(this.state.primary_selected_id);
        new_neurons[index]["layer"] = layer;
        this.setState({neurons: new_neurons})
      }
    )
  }
  remove_isi_violations = () => {
    if (this.state.primary_selected_id  === null || this.state.primary_selected_id === undefined) {
      return
    }
    /* Get all spike times for this neuron and then call modify spikes */
    get_neuron_spiketimes(this.state.primary_selected_id,
      (hash, data) => {
        let remove_indices = []
        for (var i = 0; i < data.spiketimes.length-1; i++) {
          if (data.spiketimes[i+1] - data.spiketimes[i] < 1e-3) {
            remove_indices.push(i+1)
          }
        }
        if (remove_indices.length > 0) {
          this.modify_spikes_from_neuron(remove_indices, "delete")
        }
      },
    0, this.recording_duration)
  }

  remove_cs_isi_violations = () => {
    if (this.state.primary_selected_id  === null || this.state.primary_selected_id === undefined) {
      return
    }
    /* Get all spike times for this neuron and then call modify spikes */
    get_neuron_spiketimes(this.state.primary_selected_id,
      (hash, data) => {
        let remove_indices = []
        for (var i = 0; i < data.spiketimes.length; i++) {
          for (var j = 0; j < data.cs_spiketimes.length; j++) {
            if ((data.spiketimes[i] - data.cs_spiketimes[j] > -1e-3) && (data.spiketimes[i] - data.cs_spiketimes[j] < 7.5e-3)) {
              remove_indices.push(i)
            }
          }
        }
        if (remove_indices.length > 0) {
          this.modify_spikes_from_neuron(remove_indices, "delete")
        }
      },
    0, this.recording_duration)
  }

  realign_spikes = () => {
    if (this.state.primary_selected_id  === null || this.state.primary_selected_id === undefined) {
      return
    }
    realign(this.state.primary_selected_id,
      (response) => {
        this.refresh_state();
        const selection_id = response.id;
        this.change_primary_neuron(selection_id)
      }
    );
  }

  findall_spikes = () => {
    if (this.state.primary_selected_id  === null || this.state.primary_selected_id === undefined) {
      return
    }
    findall(this.state.primary_selected_id, undefined,
      (response) => {
        this.refresh_state();
        const selection_id = response.id;
        this.change_primary_neuron(selection_id)
      }
    );
  }

  delete_cached_neuron(hash) {
    let index = this.find_neuron_index(hash, this.state.neurons);
    this.clear_selection();
    let new_neurons = this.state.neurons.slice();
    new_neurons.splice(index, 1);
    this.setState({neurons: new_neurons});
    this.delete_similarity_matrix_row_column(index);
    delete this.acg_cache[hash];
    delete this.isi_cache[hash];
    delete this.rate_cache[hash];
    delete this.template_cache[hash];
    delete this.ccg_cache[hash];
  }

  modify_spikes_from_neuron(spike_indices, type="delete") {
    if (this.state.primary_selected_id  === null || this.state.primary_selected_id === undefined) {
      return
    }
    modify_neuron(this.state.primary_selected_id, spike_indices, type,
      (response) => {
        const new_neuron_id = response.neurons[0].id;
        this.refresh_state();
        this.change_primary_neuron(new_neuron_id)
    });
  }

  handle_voltage_scroll = (e) => {
    // NOTE: This is actually on wheel not on scroll...they are different
    if (e.type == "wheel") {
      supports_wheel = true
    } else if (supports_wheel) {
      return
    }
    if (e.shiftKey) {
      e.preventDefault();
      /* Change the number of rows (if necessary) in the display */
      var num_rows = e.deltaY < 0 ? this.state.voltage_num_rows - 1 : this.state.voltage_num_rows + 1
      if (num_rows <= 0) {
        num_rows = 1
      }
      this.setState({voltage_num_rows: num_rows})
    } else if (e.altKey) {
      e.preventDefault();
      if (this.last_voltage_zoom_time === undefined || this.last_voltage_zoom_time === null || (Date.now() - this.last_voltage_zoom_time > 250)) {
        /* Throttle */
        let old_width = (this.voltage_times[1] - this.voltage_times[0]);
        let new_width = old_width * (e.deltaY > 0 ? 1.25 : 0.75);
        this.handle_voltage_percent_change(this.state.voltage_percent, new_width);
        this.last_voltage_zoom_time = Date.now();
      }
    } else if (e.ctrlKey) {
      e.preventDefault()
      if (this.state.voltage === null) {
        return;
      }
      /* TODO, Scale the voltage limits -- across all channels */
      // Find the min and max across all passed values
      var current_limits = this.state.voltage_limits
      if (this.state.voltage_limits === null || this.state.voltage_limits === undefined) {
        current_limits = this.state.voltage.y_limits
      }
      console.log(current_limits)
      if (e.deltaY > 0) {
        current_limits = [current_limits[0] * 1.10, current_limits[1] * 1.10]
      } else {
        current_limits = [current_limits[0] * 0.90, current_limits[1] * 0.90]
      }
      console.log(current_limits)
      this.setState({voltage_limits: current_limits})
    }
  }

  handle_key(e) {
    if (e.keyCode === 46 && this.state.primary_selected_id !== null) { // delete
      e.preventDefault();
      delete_neuron(this.state.primary_selected_id, () => { this.delete_cached_neuron(this.state.primary_selected_id) } );
    } else if (e.keyCode === 77 && (e.ctrlKey || e.metaKey) && this.state.primary_selected_id !== null && this.state.secondary_selected_id !== null) { // Ctrl+M
      e.preventDefault();
      this.handle_merge();
    } else if (e.keyCode === 67 && (e.ctrlKey || e.metaKey) && this.state.primary_selected_id !== null) { // Ctrl + C
      e.preventDefault();
      this.convert_neuron_type("cs")
    } else if (e.keyCode === 80 && (e.ctrlKey || e.metaKey) && this.state.primary_selected_id !== null) { // Ctrl + P
      e.preventDefault();
      this.convert_neuron_type("pc")
    } else if (e.keyCode === 82 && (e.ctrlKey || e.metaKey) && this.state.primary_selected_id !== null) { // Ctrl + R
      e.preventDefault();
      this.convert_neuron_type("neuron")
    }  else if (e.keyCode === 83 && (e.ctrlKey || e.metaKey) && this.state.primary_selected_id !== null) { // Ctrl + S
        e.preventDefault();
        this.handle_save("jld2")
    } else if ((e.ctrlKey || e.metaKey) && e.keyCode === 40) { // Ctrl+down
      e.preventDefault();
      let current_neuron_index = this.find_neuron_index(this.state.secondary_selected_id, this.sorted_hash);
      if (current_neuron_index !== null && current_neuron_index < this.state.neurons.length - 1) {
        this.change_secondary_neuron(this.sorted_hash[current_neuron_index + 1]);
      } else if (current_neuron_index === null && this.primary_selected_id !== null) {
        current_neuron_index = this.find_neuron_index(this.state.primary_selected_id, this.sorted_hash);
        if (current_neuron_index < this.state.neurons.length) {
          this.change_secondary_neuron(this.sorted_hash[current_neuron_index + 1]);
        }
      }
    } else if ((e.ctrlKey || e.metaKey) && e.keyCode === 38) { // Ctrl+up
      e.preventDefault();
      let current_neuron_index = this.find_neuron_index(this.state.secondary_selected_id, this.sorted_hash);
      if (current_neuron_index !== null && current_neuron_index > 0) {
        this.change_secondary_neuron(this.sorted_hash[current_neuron_index - 1]);
      } else if (current_neuron_index === null && this.primary_selected_id !== null) {
        current_neuron_index = this.find_neuron_index(this.state.primary_selected_id, this.sorted_hash);
        if (current_neuron_index > 0) {
          this.change_secondary_neuron(this.sorted_hash[current_neuron_index - 1]);
        }
      }
    } else if ((e.ctrlKey === false || e.ctrlKey === null) && (e.metaKey === false || e.metaKey === null) && e.keyCode === 40) { // down
      e.preventDefault()
      if (this.state.primary_selected_id == null && this.state.neurons.length > 0) {
        this.change_primary_neuron(this.sorted_hash[0]);
      } else {
        let current_neuron_index = this.find_neuron_index(this.state.primary_selected_id, this.sorted_hash);
        if (current_neuron_index !== null && current_neuron_index < this.state.neurons.length - 1) {
          this.change_primary_neuron(this.sorted_hash[current_neuron_index + 1]);
        }
      }
    } else if ((e.ctrlKey === false || e.ctrlKey === null) && (e.metaKey === false || e.metaKey === null) && e.keyCode === 38) { // up
      e.preventDefault()
      let current_neuron_index = this.find_neuron_index(this.state.primary_selected_id, this.sorted_hash);
      if (current_neuron_index !== null && current_neuron_index > 0) {
        this.change_primary_neuron(this.sorted_hash[current_neuron_index - 1]);
      }
    } else if ((e.ctrlKey === false || e.ctrlKey === null) && (e.metaKey === false || e.metaKey === null) && e.keyCode === 39) { // right
      e.preventDefault()
      if (this.voltage_times.length === 2) {
        /* Advance by 25% of the current window */
        let start_time = this.voltage_times[0] + (this.voltage_times[1] - this.voltage_times[0]) / 4
        let start_percentage = start_time * 100.0 / this.recording_duration
        this.handle_voltage_percent_change(Math.min(start_percentage, 100));
      }
    } else if ((e.ctrlKey === false || e.ctrlKey === null) && (e.metaKey === false || e.metaKey === null) && e.keyCode === 37) { // left
      e.preventDefault()
      if (this.voltage_times.length === 2) {
        /* Advance previously by 25% of the current window */
        let start_time = this.voltage_times[0] - (this.voltage_times[1] - this.voltage_times[0]) / 4
        let start_percentage = start_time * 100.0 / this.recording_duration
        this.handle_voltage_percent_change(Math.max(start_percentage, 0));
      }
    } else if ((e.ctrlKey || e.metaKey) && e.keyCode === 49) { // mark ('1' key)
      e.preventDefault()
      if (this.state.primary_selected_id !== null && this.state.secondary_selected_id === null) {
        const new_mark_list = this.state.mark_list.slice()
        if (new_mark_list.includes(this.state.primary_selected_id)) {
          let current_neuron_index = this.find_neuron_index(this.state.primary_selected_id, this.state.mark_list);
          new_mark_list.splice(current_neuron_index, 1);
        } else {
          new_mark_list.push(this.state.primary_selected_id)
        }

        this.setState({mark_list: new_mark_list})
      }
    }
  }

  check_connection_status() {
    get_session(() => {
      if (this.connection_error) {
        this.setState({modal: {visible: false}})
      }},
      () => {
        this.connection_error = true;
        clearInterval(this.check_alive_interval);
        this.setState({modal: {visible: true, header: 'Connection Error', body: 'Unable to connect to server', onClose: this.handle_load } })
        this.connection_error = false;
    })
  }

  handle_merge() {
    if (this.state.primary_selected_id === null || this.state.secondary_selected_id == null) {
      return;
    }
    if (this.state.primary_selected_id === this.state.secondary_selected_id) {
      return; // can't merge with ourselves
    }
    merge_neurons(this.state.primary_selected_id, this.state.secondary_selected_id,
      (hash_1, hash2_, response) => {
        const merged_neuron_id = response.id;
        this.refresh_state();
        this.change_primary_neuron(merged_neuron_id)
      });
  }
  handle_pc_split() {
    if (this.state.primary_selected_id === null) {
      return;
    }
    split_purkinje_cell(this.state.primary_selected_id,
      (hash, response) => {
        /* Find this neuron in our list and then delete it */
        let neurons_copy = this.state.neurons.slice()
        let index = this.find_neuron_index(this.state.primary_selected_id, neurons_copy)
        neurons_copy.splice(index, 1);
        /* Append our returned neurons to the list of neurons */
        neurons_copy.push(...response.neurons)
        this.setState({neurons: neurons_copy})
        this.clear_selection()
        get_neuron_similarity_matrix((data) => { this.setState({similarity_scores: data.values}); });
      }
    )
  }

  set_layout_manager(manager) {
    this.layout_manager = manager;
    /* TODO: These should all be registered before init() */
    /* That way popups work appropriately */
    this.layout_manager.registerComponent('ACG', () => <ACG data={this.state.acg}/>);
    this.layout_manager.registerComponent('ACGVsRate', () => <ACGVsRate data={this.state.acg_vs_rate}/>);
    this.layout_manager.registerComponent('RecordingNotes', () => <RecordingNotes notes={this.state.notes} />);
    this.layout_manager.registerComponent('RateTime', () => <RateTime data={this.state.rate}/>);
    this.layout_manager.registerComponent('ISI', () => <ISI data={this.state.isi}/>);
    this.layout_manager.registerComponent('CCG', () => <CCG data={this.state.ccg}/>);
    this.layout_manager.registerComponent('MergedACG', () => <MergedACG data={this.state.merged_acg}/>);
    this.layout_manager.registerComponent('Template', () => <Template data={this.state.template}/>);
    this.layout_manager.registerComponent('VoltagePlot', () => <VoltagePlot data={this.state.voltage} sliderChanged={this.handle_voltage_percent_change} num_rows={this.state.voltage_num_rows} recordingPercent={this.state.voltage_percent} primary_spiketimes={this.state.primary_spiketimes} secondary_spiketimes={this.state.secondary_spiketimes} onScroll={this.handle_voltage_scroll} />);
    this.layout_manager.registerComponent('PCATemplateProjection', () => <TemplateProjection projection_type="pca" data={this.state.pca_template_projection} delete_points={this.modify_spikes_from_neuron} />);
    this.layout_manager.registerComponent('UMAPTemplateProjection', () => <TemplateProjection projection_type="umap" data={this.state.umap_template_projection} delete_points={this.modify_spikes_from_neuron} />);
  }

  add_view(view_type) {
    if ((! view_type) || (! this.layout_manager)) {
      return;
    }
    let title = view_type;
    if (view_type === "ACG" || view_type === "ISI" || view_type === "CCG" || view_type === "Template") {
      title = view_type;
    } else if (view_type === 'RateTime') {
      title = "Rate vs. Time";
    } else if (view_type === 'MergedACG') {
      title = "Merged ACG";
    } else if (view_type === "ACGVsRate") {
        title = "ACG vs. Rate";
    } else if (view_type === "VoltagePlot") {
      title = "Channel Voltages"
    } else if (view_type === "PCATemplateProjection") {
      title = "PCA Template Projection"
    }
    else if (view_type === "UMAPTemplateProjection") {
      title = "UMAP Template Projection"
    } else if (view_type === "Recording Notes") {
      title = "Recording Notes"
    }
    var config = {
      type: 'react-component',
      component: view_type,
      title: title,
    }
    this.layout_manager.root.contentItems[0].addChild(config);
  }

  render() {
//https://github.com/annotationhub/react-golden-layout
    return (
      <StatusContext.Provider value={{recording_filename: this.state.recording_filename, neuron_filename: this.state.neuron_filename, primary_selected_id: this.state.primary_selected_id, secondary_selected_id: this.state.secondary_selected_id}}>
      <div style={{display: "flex", flexDirection: "column", maxHeight: "100%", height: "100%", margin: "0"}}> {/* NOTE: 100% works only if html, body has a min-height of 100% in CSS */}
        <div style={{flex: "0 1 auto", width: "100%"}} >
          <div className="neuro-vis">
            <EffectHandler unloadHandler={() => this.handle_unload()} loadHandler={() => this.handle_load()}/>
            <div className="neuronviz-menu-bar">
              <NeuroVizMenuBar neuronFileChange={this.handle_file_change}
                  handleFlatFileClicked={() => {this.flat_recording_dialog.current.set_server(false);
                    this.flat_recording_dialog.current.set_callback(this.handle_local_recording_file_change);
                    this.flat_recording_dialog.current.set_pl2(false); this.flat_recording_dialog.current.set_visible(true)}}
                  handleServerFlatFileClicked={() => {this.flat_recording_dialog.current.set_server(true);
                    this.flat_recording_dialog.current.set_callback(this.handle_server_recording_file_change);
                    this.flat_recording_dialog.current.set_pl2(false); this.flat_recording_dialog.current.set_visible(true)}}
                  handleServerPL2FileClicked={() => {this.flat_recording_dialog.current.set_server(true);
                    this.flat_recording_dialog.current.set_callback(this.handle_server_recording_file_change);
                    this.flat_recording_dialog.current.set_pl2(true); this.flat_recording_dialog.current.set_visible(true)}}
                  localRecordingFileChange={this.handle_local_recording_file_change} handle_merge={this.handle_merge} handlePCSplit={this.handle_pc_split} handle_save={this.handle_save}
                  add_view={this.add_view} convert_neuron_type={this.convert_neuron_type} label_neuron={this.label_neuron} set_neuron_layer={this.set_neuron_layer} 
		  remove_isi_violations={this.remove_isi_violations}
                  remove_cs_isi_violations={this.remove_cs_isi_violations} realign={this.realign_spikes} findall={this.findall_spikes}/>
            </div>
          </div>
        </div>
          <div style={{flex : "1 1 auto", width: "100%", marginBottom: "0px", minHeight: "0"}} onDrop={this.handle_drop} onDragOver={this.handle_drag_over} >
              <GoldenLayoutComponent htmlAttrs={{ style: { width: '100%', height: '100%' } }}
                config={{
                  settings: { showMaximiseIcon: true, showPopoutIcon: false },
                  content: [{
                    type: 'row',
                    content:[{
                        type: 'column',
                        width: 33,
                        content: [{
                          component: () => <NeuronList neurons={this.state.neurons} handle_key={this.handle_key} primary_selected_id={this.state.primary_selected_id} secondary_selected_id={this.state.secondary_selected_id} mark_list={this.state.mark_list} onClick={this.handle_neuron_click} onContextMenu={this.handle_neuron_right_click} sort_updated={this.sort_updated} />,
                          title: 'Neuron List',
                          id: 'neuron_list_container',
                          isClosable: false,
                        }, {
                        component: () => <NeuronSimilarityList neurons={this.state.neurons} primary_selected_id={this.state.primary_selected_id} secondary_selected_id={this.state.secondary_selected_id} onClick={this.handle_similarity_click} similarity_scores={this.state.similarity_scores} />,
                        title: 'Similarity',
                        id: 'neuron_list_container',
                        isClosable: false,
                        height: 25,
                      }]}, {
                        type: 'column',
                        content: [{
                          component: () => <VoltagePlot data={this.state.voltage} sliderChanged={this.handle_voltage_percent_change} limits={this.state.voltage_limits} num_rows={this.state.voltage_num_rows} recordingPercent={this.state.voltage_percent} primary_spiketimes={this.state.primary_spiketimes} secondary_spiketimes={this.state.secondary_spiketimes} onScroll={this.handle_voltage_scroll}/>,
                          title: "Channel Voltages",
                        }, {
                          type: 'row',
                          content:[{
                            component: () => <RateTime data={this.state.rate}/>,
                            title: 'Rate vs. Time'
                            }, {
                            component: () => <Template data={this.state.template} num_channels={this.state.num_channels}/>,
                            title: 'Template'
                          }]}, {
                          type: 'row',
                          content:[{
                            component: () => <ACG data={this.state.acg}/>,
                            title: 'ACG'
                          }, {
                            component: () => <ISI data={this.state.isi}/>,
                            title: 'ISI'
                          }]
                        }]
                      }]
                  }]
                }}
                // (Optional) Set up auto-resizing. Layout will resize when the window resizes.
                autoresize={true}
                // (Optional) (Milliseconds) Debounce resize to prevent excessive re-renders.
                debounceResize={100}
                // (Optional) Grab the instance of the GoldenLayout Manager. Gives you full access to GL API.
                onLayoutReady={(e) => this.set_layout_manager(e)}
              />
          </div>
          <div style={{flex: "0 1 auto", width: "100%", minHeight: "0"}} >
            <NeuroVizFooter filename={this.state.footer_text}/>
          </div>
          <Modal data={this.state.modal} />
          <FlatRecordingDialog visible={false} ref={this.flat_recording_dialog} callback={this.handle_local_recording_file_change} />
        </div>
        </StatusContext.Provider>
    );
  }
}


// ========================================

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
