import React from 'react';
import styles from './footer.module.css';

function NeuroVizFooter(props) {
  return (
    <div className={styles.footer}>
      <p><i>{ props.filename }</i></p>
    </div>
  );
}

export { NeuroVizFooter };
