import React from 'react';
import styles from './modal.module.css';

/**
 * Takes properties:
 *  header
 *  body
 *  footer
 *  onClose
 */
function Modal(props) {
  const current_modal = React.useRef(null);
  function handle_modal_click() {
    if (props.data.onClose !== undefined) {
      props.data.onClose();
    }
  };
  if (props.data.visible) {
    return (
      <div className={styles.modal} ref={current_modal}>
        <div className={styles.modal_content}>
          <span className={styles.close} onClick={handle_modal_click}>&times;</span>
          <div className={styles.modal_header}>
            {props.data.header}
          </div>
          <div className={styles.modal_body}>
            {(typeof props.data.body === "function") ? props.data.body() : props.data.body}
          </div>
          <div className={styles.modal_footer}>
            {props.data.footer}
          </div>
        </div>
      </div>
    );
  }
  return null;
}

export { Modal };
