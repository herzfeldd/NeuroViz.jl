import React from 'react';
import styles from './notes.module.css';

const RecordingNotes = (props) => {
  let temp = <h1 style={{margin: 0}}>Notes</h1>;
  if (props.notes !== undefined && props.notes !== null) {
    temp =
    <div className={styles.notes} style={{width:'100%', overflowY: 'auto', overflowX: 'auto', display: 'block'}}>
      <p style={{margin: '0pt', border: '0pt'}}>{props.notes}</p>
    </div>
  }
  return temp
}

/**
 * Returns true if the document has an ACG
 */
const has_recording_notes = () => {
  return (document.getElementsByClassName(styles.notes).length > 0)
}

export { RecordingNotes, has_recording_notes };
