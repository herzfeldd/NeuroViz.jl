import React from 'react';
import PropTypes from 'prop-types';
import modal_styles from '../modal/modal.module.css';
import styles from './flat_recording_dialog.module.css';

class FlatRecordingDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {visible: this.props.visible !== undefined ? this.props.visible : false, server: false, pl2: false};
    this.file_input = React.createRef();
    this.sampling_rate_input = React.createRef();
    this.num_channels_input = React.createRef();
    this.interleaved_input = React.createRef();
    this.server_file_name_input = React.createRef();
    this.mmap_input = React.createRef();
    this.callback = this.props.callback;
    this.file_input_value = null;
  }

  set_visible = (value, file=null) => {
    this.setState({visible: value})
    this.file_input_value = file
  }

  set_server = (value) => {
    this.setState({server: value})
  }

  set_pl2 = (value) => {
    this.setState({pl2: value})
  }

  set_callback = (value) => {
    this.callback = value
  }

  file_recording_dialog_cancel = () => {
    this.setState({visible: false})
  };
  file_recording_dialog_ok = () => {
    let is_valid = true;
    if (this.state.server === false && this.file_input_value === null && this.file_input.current.files.length === 0) {
      this.file_input.current.className = styles.invalid;
      is_valid = false;
    }
    if (this.state.server === true && this.server_file_name_input.current.value.length === 0) {
      this.server_file_name_input.file_input.current.className = styles.invalid;
      is_valid = false;
    }
    if (this.state.pl2 === false && this.sampling_rate_input.current.value.length === 0) {
      this.sampling_rate_input.current.className = styles.invalid;
      is_valid = false;
    }
    if (this.state.pl2 === false && this.num_channels_input.current.value.length === 0) {
      this.num_channels_input.current.className = styles.invalid;
      is_valid = false;
    }
    if (is_valid && this.props.callback !== undefined) {
      this.setState({visible: false})
      if (this.state.pl2) {
        this.callback(this.state.server ? this.server_file_name_input.current.value : this.file_input.current.files, "pl2")
      } else {
        this.callback(this.state.server ? this.server_file_name_input.current.value : (this.file_input_value === null ? this.file_input.current.files : this.file_input_value), "flat", this.num_channels_input.current.value, this.sampling_rate_input.current.value, this.interleaved_input.current.checked, this.mmap_input.current.checked)
      }
    }
  };

  remove_invalid_style = (e) =>  {e.target.className = ""}

  render() {
    if (! this.state.visible) {
      return ( null );
    }
    return (
      <div className={modal_styles.modal}>
        <div className={modal_styles.modal_content}>
          <span className={modal_styles.close}  onClick={this.file_recording_dialog_cancel}>&times;</span>
          <div className={modal_styles.modal_header}>
          { (this.state.pl2 === false) && "Flat Recording Parameters" }
          { (this.state.pl2 === true) && "PL2 Path" }
          </div>
          <div className={modal_styles.modal_body}>
            <div className={styles.parameter}>
              <label>Recording file:</label>
              { (this.state.server === false && this.file_input_value === null) && <input type="file" ref={this.file_input} accept=".bin,.dat" onChange={this.remove_invalid_style} value={this.file_input_value}/> }
              { (this.state.server === true) && <input type="text" style={{fontSize: "10pt", width: "100%"}} ref={this.server_file_name_input} onChange={this.remove_invalid_style}/> }
              { (this.state.server === false && this.file_input_value !== null) && <label style={{fontWeight: "normal"}}>{this.file_input_value[0].name}</label> }
            </div>
            { (this.state.pl2 === false) &&
              <div className={styles.parameter}>
                <label>Number of channels:</label>
                <input ref={this.num_channels_input} onChange={this.remove_invalid_style} type="number" min="1" max="1000" placeholder="1"/>
              </div> }
            { (this.state.pl2 === false) &&
              <div className={styles.parameter}>
                <label>Sampling rate:</label>
                <input ref={this.sampling_rate_input} onChange={this.remove_invalid_style} type="number" min="1" max="200000" placeholder="40000"/>
              </div> }
            { (this.state.pl2 === false) && <div className={styles.parameter}>
                <label>Interleaved?</label>
                <input ref={this.interleaved_input} type="checkbox"/>
              </div> }
            { (this.state.pl2 === false) && <div className={styles.parameter}>
                <label>Mmap?</label>
                <input ref={this.mmap_input} type="checkbox"/>
              </div> }
          </div>
          <div className={modal_styles.modal_footer}>
            <button onClick={this.file_recording_dialog_ok}>OK</button>
            <button onClick={this.file_recording_dialog_cancel}>Cancel</button>
          </div>
        </div>
      </div>
    );
  }
}

FlatRecordingDialog.propTypes = {
  visible: PropTypes.bool,
  callback: PropTypes.func.isRequired
}

export { FlatRecordingDialog };
