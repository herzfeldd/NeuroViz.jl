import React from 'react';
import { common_line_plot_parameters } from './common.js';
import { ResponsiveLine } from '@nivo/line';

// Used for a div wrapper around the plot
const class_name = "merged-acg-plot";

const MergedACG = (props) => {
  let temp = <h1 style={{margin: 0}}>ACG</h1>;
  if (props.data !== undefined && props.data !== null && props.data.length > 0 && props.data[0] != null && props.data[0].data != null && props.data[0].data.length > 0) {
    temp = <ResponsiveLine
        data = {props.data}
        axisBottom={{
          tickValues: 5,
          format: value => Number(value).toFixed(1),
          tickSize: 5,
          tickPadding: 10,
          tickRotation: 0,
          legend: 'Time (ms)',
          legendPosition: 'middle',
          legendOffset: 40
        }}
        axisLeft={{
          tickSize: 5,
          tickPadding: 10,
          tickRotation: 0,
          legend: 'Firing rate (Hz)',
          legendPosition: 'middle',
          legendOffset: -40
        }}
        xScale={{ type: 'linear', min: props.data[0].limits[0], max: props.data[0].limits[1] }}
        {...common_line_plot_parameters}
        enableArea={true}
        areaOpacity={0.8}
        colors={"purple"}
      />
  }
  return <div className={class_name}>{temp}</div>;
}

/**
 * Returns true if the document has an ACG
 */
const has_merged_acg = () => {
  return (document.getElementsByClassName(class_name).length > 0)
}

export { MergedACG, has_merged_acg };
