import React from 'react';
import PropTypes from 'prop-types';

import { WebGLPlot, WebglLine, ColorRGBA } from "webgl-plot";

// Used for a div wrapper around the plot
const class_name = "voltage-plot";

const voltage_slider_style = {width: "100%", position: "sticky", top: "0", zIndex: "2"};

class VoltagePlot extends React.Component {
  constructor(props) {
    super(props);
    this.view_ref = React.createRef()
    this.canvas_ref = React.createRef()
    this.web_gl_plot = null;
    this.handle_resize = this.handle_resize.bind(this);
    this.prevent_default = (e) => { if (e.altKey) { e.preventDefault() } }
    this.timer = null;
  }

  componentDidUpdate() {
    if (this.canvas_ref.current === null) {
      this.web_gl_plot = null; /* Null the reference so that the web gl plot doesn't refer to a previously destroyed canvas */
      return;
    }
    if (this.web_gl_plot === null) {
      const devicePixelRatio = window.devicePixelRatio || 1;
      this.canvas_ref.current.width = this.canvas_ref.current.clientWidth * devicePixelRatio;
      this.canvas_ref.current.height = this.canvas_ref.current.clientHeight * devicePixelRatio;
      this.web_gl_plot = new WebGLPlot(this.canvas_ref.current);
    }
    this.web_gl_plot.removeAllLines()
    this.set_data()
  }

  componentDidMount() {
    window.addEventListener("resize", this.handle_resize, false);
    this.view_ref.current.addEventListener("keyup", this.prevent_default, false);
    this.view_ref.current.addEventListener("wheel", this.props.onScroll, false);
    this.view_ref.current.addEventListener("mousewheel", this.prevent_default, false)
    this.view_ref.current.addEventListener("DOMMouseScroll", this.prevent_default, false)
    this.view_ref.current.addEventListener("MozMousePixelScroll", this.prevent_default, false)
    this.timer = setInterval(this.handle_resize, 300);
  }
  componentWillUnmount() {
    this.view_ref.current.removeEventListener("keyup", this.prevent_default, false);
    this.view_ref.current.removeEventListener("wheel", this.props.onScroll, false);
    this.view_ref.current.removeEventListener("mousewheel", this.prevent_default, false)
    this.view_ref.current.removeEventListener("DOMMouseScroll", this.prevent_default, false)
    this.view_ref.current.removeEventListener("MozMousePixelScroll", this.prevent_default, false)
    clearInterval(this.timer);
  }
  handle_resize() {
    const devicePixelRatio = window.devicePixelRatio || 1;
    if (this.canvas_ref.current && this.web_gl_plot && ((this.canvas_ref.current.width !== this.canvas_ref.current.clientWidth) || (this.canvas_ref.current.height !== this.canvas_ref.current.clientHeight))) {
      this.canvas_ref.current.width = this.canvas_ref.current.clientWidth * devicePixelRatio;
      this.canvas_ref.current.height = this.canvas_ref.current.clientHeight * devicePixelRatio;
      this.web_gl_plot.viewport(0, 0, this.canvas_ref.current.width, this.canvas_ref.current.height)
      this.web_gl_plot.clear()
      this.web_gl_plot.update();
    }
  }
  set_data() {
    if (this.props.data.y.length === 0) {
      return;
    }
    for (var channel_number = 0; channel_number < this.props.data.y.length; channel_number++) {
      const num_x = this.props.data.y[channel_number].length;
      const range = (this.props.limits === null || this.props.limits === undefined) ? this.props.data.y_limits[1] - this.props.data.y_limits[0] : this.props.limits[1] - this.props.limits[0]
      const offset = (this.props.limits === null || this.props.limits === undefined) ? this.props.data.y_limits[0] : this.props.limits[0]
      let voltage_line = new WebglLine(new ColorRGBA(1, 1, 1, 1), num_x);

      voltage_line.lineSpaceX(-1, 2 / num_x);
      for (var i = 0; i < voltage_line.numPoints; i++) {
        // Ydata must be scaled from -1 (min) to +1 (max)
        let current_y_point = 2.0 * (((this.props.data.y[channel_number][i] - offset) / range) - 0.5);

        /* Current y point is scaled between -1 and + 1 corresponding to the full view port. To put these
        in individual plots, we need to divide by the the total number of series and shift and y value based on the offset
        for this channel */
        current_y_point = current_y_point / this.props.data.y.length; /* Divide by number of channels to display */
        // Zero here is the center of the canvas, + 1 is the top of the canvas and -1 is the
        // bottom of the canvas
        current_y_point = current_y_point + (1 - (2 * (channel_number) / (this.props.data.y.length)) - 1/this.props.data.y.length)
        voltage_line.setY(i, current_y_point)
      }
      this.web_gl_plot.addLine(voltage_line) // this needs to be added first (so that it is under everything else)
      var j;
      // // Create new lines for each primary spike time (blue)
      const one_ms_indices = Math.ceil(1.0e-3 / this.props.data.dt)
      for (i = 0; i < this.props.primary_spiketimes.length; i++) {
        const start_index = Math.floor((this.props.primary_spiketimes[i] - this.props.data.start_time) / (this.props.data.dt)) - one_ms_indices
        if (start_index < 0 || start_index + 2 * one_ms_indices > num_x) {
          continue;
        }
        let current_line = new WebglLine(new ColorRGBA(0.5, 0.8, 1, 1), one_ms_indices * 2)
        this.web_gl_plot.addLine(current_line)
        for (j = 0; j < one_ms_indices * 2; j++) {
          current_line.setX(j, voltage_line.getX(j + start_index))
          current_line.setY(j, voltage_line.getY(j + start_index))
        }
      }
      // Create new lines for each secondary spike time (red)
      var primary_index = 0;
      for (i = 0; i < this.props.secondary_spiketimes.length; i++) {
        const start_index = Math.floor((this.props.secondary_spiketimes[i] - this.props.data.start_time) / (this.props.data.dt)) - one_ms_indices
        if (start_index < 0 || start_index + 2 * one_ms_indices > num_x) {
          continue;
        }

        /* Check if this index overlaps with any of the primary indices */
        var overlaps = false;
        while (primary_index < this.props.primary_spiketimes.length) {
          if (Math.abs(this.props.primary_spiketimes[primary_index] - this.props.secondary_spiketimes[i]) < 1e-3) {
            overlaps = true
          }
          if (this.props.primary_spiketimes[primary_index] > this.props.secondary_spiketimes[i] + 0.5e-3) {
            break;
          }
          primary_index = primary_index + 1;
        }
        var current_line;
        if (overlaps === false) {
          current_line = new WebglLine(new ColorRGBA(1, 0.3, 0.3, 1), one_ms_indices * 2)
        } else {
          current_line = new WebglLine(new ColorRGBA(1, 0, 1, 1), one_ms_indices * 2) /* Purple */
        }
        this.web_gl_plot.addLine(current_line)
        for (j = 0; j < one_ms_indices * 2; j++) {
          current_line.setX(j, voltage_line.getX(j + start_index))
          current_line.setY(j, voltage_line.getY(j + start_index))
        }
      }

    }
    this.web_gl_plot.update();
  }

  render() {
    let temp = <h1 style={{margin: 0}}>Voltage</h1>;
    if (this.props.data !== null && this.props.data.y !== null && this.props.data.y.length > 0 ) {
      const label_series = []
      for (var i = 0; i < this.props.data.y.length; i++) {
          label_series.push(<div key={"div_channel_" + i} style={{gridColumnStart: 1, gridColumnEnd: 2, gridRowStart: i+1, gridRowEnd: i+2}}><h2  style={{color: "white"}}>{i+1}</h2></div>);
          //line_series.push(<ChannelVoltagePlot key={"voltage_channel_" + i} primary_spiketimes={props.primary_spiketimes} secondary_spiketimes={props.secondary_spiketimes} start_time={props.data.start_time} dt={props.data.dt} yData={props.data.y[i]} scaling={props.data.y_limits}/>);
        }
        //const column_settings = "100%";
        const column_settings = "repeat(1, 15px auto)"
        const row_settings = "repeat(" + this.props.data.y.length + ", " + Math.max(100 / this.props.data.y.length, 100 / this.props.num_rows) + "%)";
      temp = <div>
        <input type={"range"} min={0} max={100} style={voltage_slider_style} onChange={(e) => this.props.sliderChanged(e.target.value)} value={this.props.recordingPercent}/>
        <div style={{width: "100%", height: "100%", position: "absolute", top: "0", overflowY: "auto", display: "grid",
          minWidth: "0", minHeight: "0", margin: "0", padding: "0",
          gridColumnGap: "10px", gridRowGap: "0", gridTemplateColumns:
          column_settings, gridTemplateRows: row_settings }}>
        {label_series}
        <canvas ref={this.canvas_ref} style={{width: "100%", height: "100%", gridColumnStart: 2, gridColumnEnd: 3, gridRowStart: 1, gridRowEnd: this.props.data.y.length + 1}} />
        </div>
      </div>
    }
    return <div className={class_name} ref={this.view_ref} tabIndex="0">{temp}</div>;
  }
}
VoltagePlot.propTypes = {
  sliderChanged: PropTypes.func.isRequired,
  recordingPercent: PropTypes.number.isRequired,
  primary_spiketimes: PropTypes.array,
  secondary_spiketimes: PropTypes.array,
  onScroll: PropTypes.func.isRequired,
  num_rows: PropTypes.number
}
VoltagePlot.defaultProps = {
  num_rows: 4
};

/**
 * Returns true if the document has an ACG
 */
const has_voltage_plot = () => {
  return (document.getElementsByClassName(class_name).length > 0)
}

/**
 * Convert a response from the server to the types that are expected by
 * the nivo plot.
 *
 * This can be used as the callback to services/get_neuron_acg which, on
 * success, calls the callback with the hash of the neuron that we called for,
 * the JSON response.
 */
const response_to_voltage_data = (response) => {
  /**
   * The returned response contains
   * dt: A 1xN vector of the time step of all voltage timeseries
   * limits: The Nx2 vector of start and end times
   * channels: A 1xN vector of channel indices
   * voltage: A NxNt matrix of channel voltages
   */
  const voltage = response.voltage

  // Find the min and max across all passed values
  let max_value = -Infinity
  let min_value = Infinity
  for (var i = 0; i < voltage.length; i++) {
    min_value = Math.min(...voltage[i], min_value)
    max_value = Math.max(...voltage[i], max_value)
  }

  const parsed_values = Object()
  parsed_values.y_limits = [min_value, max_value]
  parsed_values.y = voltage
  parsed_values.start_time = response.start_time
  parsed_values.dt = response.dt[0]

  return parsed_values
}

export { VoltagePlot, has_voltage_plot, response_to_voltage_data };
