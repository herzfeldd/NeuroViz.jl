import React from 'react';
import createScatterplot from 'regl-scatterplot';
import PropTypes from 'prop-types';

// Used for a div wrapper around the plot
const class_name = "template-projection-plot";

class TemplateProjection extends React.Component {
  constructor(props) {
    super(props);
    this.canvas_ref = React.createRef()
    this.view_ref = React.createRef()
    this.scatterplot = null;
    this.update_plot = this.update_plot.bind(this);
    this.handle_resize = this.handle_resize.bind(this);
    this.handle_key = this.handle_key.bind(this);
    this.selected_points = []
    this.timer = null;
  }
  componentDidMount() {
    window.addEventListener("resize", this.handle_resize, false);
    this.canvas_ref.current.oncontextmenu = (e) => { console.log("HERE"); e.preventDefault() }
    this.view_ref.current.addEventListener("keydown", this.handle_key);
    this.timer = setInterval(this.handle_resize, 300);
    this.handle_resize()
  }
  componentDidUpdate() {
    if (Array.isArray(this.props.data)) {
      this.update_plot()
    } else if (this.scatterplot !== null && this.scatterplot !== undefined) {
      this.selected_points = []
      this.scatterplot.draw([])
    }
  }
  componentWillUnmount() {
    this.view_ref.current.removeEventListener("keydown", this.handle_key);
    clearInterval(this.timer);
  }

  handle_key(e) {
    e.preventDefault()
    if (e.keyCode === 46 && this.selected_points.length > 0 && this.selected_points.length < this.props.data.length) {
      this.props.delete_points(this.selected_points, "delete")
      this.selected_points = []
      this.scatterplot.deselect()
    } else if (e.keyCode === 83 && this.selected_points.length > 0 && this.selected_points.length < this.props.data.length) {
      this.props.delete_points(this.selected_points, "split")
      this.selected_points = []
      this.scatterplot.deselect()
    }
  }

  update_plot() {
    if (this.scatterplot === null) {
        this.scatterplot = createScatterplot({
          canvas: this.canvas_ref.current,
          width: this.canvas_ref.current.width,
          height: this.canvas_ref.current.height,
          backgroundColor: [0, 0, 0, 0], // Clear
          pointColor: [114/255, 220/255, 252/255, 1], //Blueish
          lassoColor: [1, 1, 1, 1], // White
          pointColorActive: [0, 1, 1, 1],
          pointOutlineWidth: 1,
          //xScale: xScale,
          //yScale: yScale,
          //cameraTarget: [xMin + (xMax - xMin) / 2, yMin + (yMax - yMin) / 2],
          //aspectRatio: (xMax - xMin) / (yMax - yMin),
          aspectRatio: this.canvas_ref.current.width / this.canvas_ref.current.height,
          //lassoClearEvent: 'deselect',
          pointSize: 5},
        );
        this.scatterplot.subscribe("select", (points) => { this.selected_points = points.points.slice(); });
        this.scatterplot.subscribe("deselect", () => { this.selected_points = []; });
        this.scatterplot.draw(this.props.data);
      } else {
        const { width, height } = this.canvas_ref.current.getBoundingClientRect();
        this.scatterplot.set({width: width, height: height})
        this.scatterplot.set({aspectRatio: this.canvas_ref.current.width / this.canvas_ref.current.height})
        this.scatterplot.reset()
        this.scatterplot.draw(this.props.data);
      }
  }
  handle_resize() {
    if (this.view_ref.current === null) {
      return;
    }
    const devicePixelRatio = window.devicePixelRatio || 1;
    const width = this.view_ref.current.clientWidth * devicePixelRatio
    const height = this.view_ref.current.clientHeight * devicePixelRatio
    if ((width === this.canvas_ref.current.width && height === this.canvas_ref.current.height) || ((width + "px") === this.canvas_ref.current.style.width && (height + "px") === this.canvas_ref.current.style.height)) {
      return;
    }

    this.canvas_ref.current.width = width;
    this.canvas_ref.current.height = height;
    this.canvas_ref.current.style.width = width + 'px';
    this.canvas_ref.current.style.height = height + 'px';
    if (Array.isArray(this.props.data)) {
      this.update_plot()
    } else if (this.scatterplot !== null && this.scatterplot !== undefined) {
      this.selected_points = []
      this.scatterplot.draw([])
    }
  }
  render() {
    // Note that tabIndex is crucial here to be able to get key events/focusable
    // //
    return (
      <div ref={this.view_ref} className={class_name} tabIndex="-1" template-projection-type={this.props.projection_type}>
        <canvas ref={this.canvas_ref} tsyle={{display: "none"}} style={Array.isArray(this.props.data) === false ? {display: "none"} : {}} />
        <div className={"loader"} style={Array.isArray(this.props.data) ? {display: "none"} : {width: "25px", height: "25px"}} />
      </div>
    );
  }
}
TemplateProjection.propTypes = {
  //data: PropTypes.array,
  projection_type: PropTypes.string.isRequired,
  delete_points: PropTypes.func.isRequired,
};
TemplateProjection.defaultProps = {
  data: [],
  projection_type: "pca",
};

/**
 * Returns true if the document has an ACG
 */
const has_template_projection = () => {
  return (document.getElementsByClassName(class_name).length > 0)
}

export const has_umap_template_projection = () => {
  return (document.querySelectorAll('[template-projection-type="umap"]').length > 0)
}

export const has_pca_template_projection = () => {
  return (document.querySelectorAll('[template-projection-type="pca"]').length > 0)
}

const response_to_template_projection_data = (response) => {
  console.log(response)
  const x = response.x;
  const y = response.y

  var points = x.map((x, i) => ([x, y[i]]))

  // Scale to (-1 to +1) in both directions
  scale_data(points);

  return points
}


/**
 * Scale any input data (list of [x, y] points)
 * to -1, 1 in both dimensions
 */
const scale_data = (points) => {
  var xMin = Infinity
  var xMax = -Infinity
  var yMin = Infinity
  var yMax = -Infinity
  for (var i = 0; i < points.length; i++) {
    if (points[i][0] < xMin) {
      xMin = points[i][0]
    }
    if (points[i][0] > xMax) {
      xMax = points[i][0]
    }
    if (points[i][1] < yMin) {
      yMin = points[i][1]
    }
    if (points[i][1] > yMax) {
      yMax = points[i][1]
    }
  }

  // Rescale all points to be between -1 and 1
  for (i = 0; i < points.length; i++) {
    points[i][0] = ((points[i][0] - xMin) * 2 / (xMax - xMin)) -1
    points[i][1] = ((points[i][1] - yMin) * 2 / (yMax - yMin)) - 1
  }
}

export { TemplateProjection, response_to_template_projection_data, has_template_projection };
