import React from 'react';
import { common_line_plot_parameters } from './common.js';
import { ResponsiveLine } from '@nivo/line';
//import AutoSizer from "react-virtualized-auto-sizer";

// Used for a div wrapper around the plot
const class_name = "template-plot";

const Template = (props) => {
  let temp = <h1 style={{margin: 0}}>Templates</h1>;
  if (props.num_channels > 0 && props.data !== undefined && props.data !== null && props.data.length > 0 ) {
    const num_cols = Math.ceil(Math.sqrt(props.num_channels));
    const num_rows = Math.ceil(props.num_channels / num_cols)
    const line_series = []
    for (var i = 0; i < props.num_channels; i++) {
      line_series.push(<ResponsiveLine
          key={i}
          data = {props.data[i]}
          yScale={{ type: 'linear', min: props.data[i][0].y_limits[0], max: props.data[i][0].y_limits[1]}}
          xScale={{ type: 'linear', min: props.data[i][0].x_limits[0], max: props.data[i][0].x_limits[1] }}
          {...common_line_plot_parameters}
          isInteractive={false}
          lineWidth={2}
          useMesh={false}
          axisLeft={null}
          axisBottom={null}
          enableGridY={false}
          colors={["#72dcfc", "red"]}
          margin={{top: 0, bottom: 0, left: 0, right: 0}}
        />)
      }
      const column_settings = "repeat(" + num_cols + ", " + (100 / num_cols) + "%)";
      const row_settings = "repeat(" + num_cols + ", " + (100 / num_rows) + "%)";
    temp = <div style={{width: "100%", height: "100%", display: "grid", minWidth: "0", minHeight: "0", margin: "0", padding: "0", gridColumnGap: "0", gridRowGap: "0", gridTemplateColumns: column_settings, gridTemplateRows: row_settings }}>{line_series}</div>

  }
  return <div className={class_name}>{temp}</div>;
}

/**
 * Returns true if the document has an ACG
 */
const has_template = () => {
  return (document.getElementsByClassName(class_name).length > 0)
}

/**
 * Convert a response from the server to the types that are expected by
 * the nivo plot.
 *
 * This can be used as the callback to services/get_neuron_acg which, on
 * success, calls the callback with the hash of the neuron that we called for,
 * the JSON response.
 */
const response_to_template_data = (response, index=0, old_rate=[[{data: null, id: null}]]) => {
  const dt = response.dt
  const values = response.values;

  // Find the min and max across all passed values
  let max_value = -Infinity
  let min_value = Infinity
  for (var i = 0; i < values.length; i++) {
    min_value = Math.min(...values[i], min_value)
    max_value = Math.max(...values[i], max_value)
  }

  if (! old_rate) {
    old_rate = [[{data: null, id: null}]];
  }

  let new_template = JSON.parse(JSON.stringify(old_rate));

  for (i = 0; i < values.length; i++) {
    if (new_template[i] === undefined) {
      new_template.push([Object()])
    } else {
      while (new_template[i].length <= index) {
        new_template[i].push({data: null, id: null})
      }
    }
    new_template[i][index].data = values[i].map((y, i) => ({x: ((i * dt)).toString(), y: y}));
    new_template[i][index].id = "Channel_" + i + "_" + index + "_" + Math.ceil(Math.random() * 1000);
    new_template[i][0].x_limits = [0, values[i].length * dt]
    if (new_template[i][0].y_limits !== undefined) {
      min_value = Math.min(new_template[i][0].y_limits[0], min_value)
      max_value = Math.max(new_template[i][0].y_limits[1], max_value)
    }
    new_template[i][0].y_limits = [min_value, max_value]
  }
  return new_template
}

export { Template, has_template, response_to_template_data };
