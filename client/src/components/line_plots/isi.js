import React from 'react';
import { common_line_plot_parameters } from './common.js';
import { ResponsiveLine } from '@nivo/line';

// Used for a div wrapper around the plot
const class_name = "isi-plot";

const ISI = (props) => {
  let temp = <h1 style={{margin: 0}}>ISI</h1>;
  if (props.data !== undefined && props.data !== null && props.data.length > 0 && props.data[0] != null && props.data[0].data != null && props.data[0].data.length > 0) {
    temp = <ResponsiveLine
        data = {props.data}
        axisBottom={{
          tickValues: 5,
          format: value => Number(value).toFixed(1),
          tickSize: 5,
          tickPadding: 10,
          tickRotation: 0,
          legend: 'Time (ms)',
          legendPosition: 'middle',
          legendOffset: 40
        }}
        axisLeft={{
          tickSize: 5,
          tickPadding: 10,
          tickRotation: 0,
          legend: 'Probability (x100)',
          legendPosition: 'middle',
          legendOffset: -40
        }}
        xScale={{ type: 'linear', min: props.data[0].limits[0], max: props.data[0].limits[1] }}
        {...common_line_plot_parameters}
        enableArea={true}
        areaOpacity={0.8}
        colors={["#72dcfc", "red"]}
      />
  }
  return <div className={class_name}>{temp}</div>;
}

/**
 * Returns true if the document has an ACG
 */
const has_isi = () => {
  return (document.getElementsByClassName(class_name).length > 0)
}

/**
 * Convert a response from the server to the types that are expected by
 * the nivo plot.
 *
 * This can be used as the callback to services/get_neuron_acg which, on
 * success, calls the callback with the hash of the neuron that we called for,
 * the JSON response.
 */
const response_to_isi_data = (response, index=0, old_isi=[{data: null, id: null, limits: null}]) => {
  const x_min = response.limits[0]
  const x_max = response.limits[1]
  const dt = response.dt
  const values = response.values;

  if (! old_isi) {
    old_isi = [{data: null, id: null, limits: null}]
  }

  let new_isi = JSON.parse(JSON.stringify(old_isi));
  while (new_isi.length <= index) {
    new_isi.push({data: null, id: null, limits: null});
  }

  /* Create a new state */
  new_isi[index].data = values.map((y, i) => ({x: ((i * dt + x_min) * 1000).toString(), y: y * 100}));
  new_isi[index].id = "ISI_" + Math.ceil(Math.random() * 1000);
  new_isi[index].limits = [x_min * 1000, x_max * 1000]

  return new_isi
}

export { ISI, response_to_isi_data, has_isi };
