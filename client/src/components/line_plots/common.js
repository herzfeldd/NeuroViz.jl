
const theme = {
  fontSize: 11,
  textColor: '#eee', /* A little off white */
    axis: {
        domain: { /* Actual axis x and y lines */
            line: {
                stroke: '#fff', /* White */
                strokeWidth: 2
            }
        }
    },
    grid: {
      line: {
          stroke: '#eeeeee33', /* Off white but with 30% opacity */
          strokeWidth: 1
      }
    }
}

/**
 * Defines common parameters of line plots (colors, etc)
 * These can be overridden after specifying {...common_line_parameters}
 */
const common_line_plot_parameters = {
  margin: {
    "top": 10,
    "right": 20,
    "bottom": 50,
    "left": 50
  },
  lineWidth: 1,
  animate: true,
  curve: "linear", /* Do not smoothly interpolate between points */
  enableGridX: false, /* No x grid */
  enablePoints: false, /* Do not show dots of individual data points */
  axisTop: null, /* No axis on the top of the plot */
  isInteractive: true, /* Allow hover */
  enableArea: false, /* Area underneighth the curve */
  colors: "white", /* Default color of line (should be overriden) */
  useMesh: true, /* Interaction method see nivo for more details */
  theme: theme, /* Use the dark them described above */
}

export { common_line_plot_parameters, };
