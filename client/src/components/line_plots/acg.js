import React from 'react';
import { common_line_plot_parameters } from './common.js';
import { ResponsiveLine } from '@nivo/line';

// Used for a div wrapper around the plot
const class_name = "acg-plot";

const ACG = (props) => {
  let temp = <h1 style={{margin: 0}}>ACG</h1>;
  if (props.data !== undefined && props.data !== null && props.data.length > 0 && props.data[0] != null && props.data[0].data != null && props.data[0].data.length > 0) {
    temp = <ResponsiveLine
        data = {props.data}
        axisBottom={{
          tickValues: 5,
          format: value => Number(value).toFixed(1),
          tickSize: 5,
          tickPadding: 10,
          tickRotation: 0,
          legend: 'Time (ms)',
          legendPosition: 'middle',
          legendOffset: 40
        }}
        axisLeft={{
          tickSize: 5,
          tickPadding: 10,
          tickRotation: 0,
          legend: 'Firing rate (Hz)',
          legendPosition: 'middle',
          legendOffset: -40
        }}
        xScale={{ type: 'linear', min: props.data[0].limits[0], max: props.data[0].limits[1] }}
        {...common_line_plot_parameters}
        enableArea={true}
        areaOpacity={0.8}
        colors={["#72dcfc", "red"]}
      />
  }
  return <div className={class_name}>{temp}</div>;

}

/**
 * Returns true if the document has an ACG
 */
const has_acg = () => {
  return (document.getElementsByClassName(class_name).length > 0)
}

/**
 * Convert a response from the server to the types that are expected by
 * the nivo plot.
 *
 * This can be used as the callback to services/get_neuron_acg which, on
 * success, calls the callback with the hash of the neuron that we called for,
 * the JSON response.
 */
const response_to_acg_data = (response, index=0, old_acg=[{data: null, id: null, limits: null}]) => {
  const x_min = response.limits[0]
  const x_max = response.limits[1]
  const dt = response.dt
  const values = response.values;
  if (! old_acg) {
    old_acg = [{data: null, id: null, limits: null}];
  }

  let new_acg = JSON.parse(JSON.stringify(old_acg));
  while (new_acg.length <= index) {
    new_acg.push({data: null, id: null, limits: null});
  }

  /* Create a new state */
  new_acg[index].data = values.map((y, i) => ({x: ((i * dt + x_min) * 1000).toString(), y: y / dt}));
  new_acg[index].id = "ACG_" + Math.ceil(Math.random() * 1000);
  new_acg[index].limits = [x_min * 1000, x_max * 1000]

  return new_acg
}

export { ACG, response_to_acg_data, has_acg };
