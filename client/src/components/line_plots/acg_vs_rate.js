import React from 'react';
import { common_line_plot_parameters } from './common.js';
import { ResponsiveHeatMap } from '@nivo/heatmap';

// Used for a div wrapper around the plot
const class_name = "acg-rate-plot";

const ACGVsRate = (props) => {
  let temp = <h1 style={{margin: 0}}>ACG vs. Rate</h1>;
  if (props.data !== undefined && props.data !== null && props.data.data != null && props.data.data.length > 0) {
    temp = <ResponsiveHeatMap
        data = {props.data.data}
        keys = {props.data.keys}
        indexBy = {props.data.index_by}
        margin = {{
          top: 10,
          right: 20,
          bottom: 50,
          left: 50
        }}
        enableGridX = {false}
        enableGridY = {false}
        axisRight={null}
        axisTop={null}
        axisBottom={{
          tickValues: 5,
          format: value => (Math.floor(value) % 50 === 0) ? Number(value).toFixed(1) : "",
          tickSize: 5,
          tickPadding: 10,
          tickRotation: 0,
          legend: 'Time (ms)',
          legendPosition: 'middle',
          legendOffset: 40
        }}
        axisLeft={{
          tickSize: 5,
          format: value => Number(value).toFixed(1),
          tickPadding: 10,
          tickRotation: 0,
          legend: 'Firing rate (Hz)',
          legendPosition: 'middle',
          legendOffset: -40
        }}
        {...common_line_plot_parameters}
        colors = "spectral"
        animate = {false}
        isInteractive = {false}
        enableLabels = {false}
      />
  }
  return <div className={class_name}>{temp}</div>;

}
/**
 * Returns true if the document has an ACG
 */
const has_acg_vs_rate = () => {
  return (document.getElementsByClassName(class_name).length > 0)
}


/**
 * Convert a response from the server to the types that are expected by
 * the nivo plot.
 *
 * This can be used as the callback to services/get_neuron_acg which, on
 * success, calls the callback with the hash of the neuron that we called for,
 * the JSON response.
 */
const response_to_acg_vs_rate_data = (response) => {
  const x_min = response.limits[0]
  const x_max = response.limits[1]
  const dt = response.dt
  let new_acg = {data: null, index_by: null, limits: null};

  /* Create a new state */
  var data = []
  for (let i = response.bins.length - 1; i >= 0; i--) {
    let current_row = {"id": response.bins[i]}
    for (let j = 0; j < response.values[i].length; j++) {
      // Given that we don't care about the actual numbers, we multiply by -1
      // here so that the heatmap (spectral) goes from blues to reds. If we ever
      // end up plotting the z-axis, this should be removed and we should
      // find a better way to reverse the spetral heatmap (which goes from low values = red
      // to high values = blue)
      current_row[Math.round((x_min + j * dt) * 1000)] = -1.0 * response.values[i][j]
    }
    data.push(current_row)
  }
  new_acg.data = data;
  //new_acg[index].id = "ACGVsRate_" + index;
  new_acg.limits = [x_min * 1000, x_max * 1000]
  new_acg.index_by = "id"
  new_acg.keys = response.values[0].map((y, i) => Math.round((i * dt + x_min) * 1000).toString());

  return new_acg
}

export { ACGVsRate, response_to_acg_vs_rate_data, has_acg_vs_rate };
