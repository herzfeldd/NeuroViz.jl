import React from 'react';
import styles from './progressbar.module.css';

/**
 * Takes properties:
 * value (a value between 0 and 100)
 */
function ProgressBar(props) {
  if (props.value === undefined) {
    props.value = 0;
  }
  return (
    <div className={styles.progressbar}>
      <div className={styles.progressbar_interior} style={{width: props.value + "%"}}>
        <div>{Number(props.value).toFixed(1)}%</div>
      </div>
    </div>
  );

}

function IndefiniteProgressBar(props) {
  return (
    <div className={styles.progressbar_continuous}>
      <div className={`${styles.progressbar_interior} ${styles.inc}`}></div>
    </div>
  );
}

export { ProgressBar, IndefiniteProgressBar };
