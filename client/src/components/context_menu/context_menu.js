import React from 'react';
import PropTypes from 'prop-types';
import styles from './context_menu.module.css';

class ContextMenu extends React.Component {
    constructor(props){
      super(props);
      this.state = {xPos: "0px", yPos: "0px:", showMenu: false}
    }

    componentDidMount() {
        document.addEventListener("click", this.handleClick);
    }
    componentWillUnmount() {
        document.removeEventListener("click", this.handleClick);
    }
    handleClick = (e) => {
        if (this.state.showMenu) {
          this.setState({ showMenu: false });
        }
    }
    handleContextMenu = (e) => {
        e.preventDefault();
        this.setState({xPos: e.pageX + "px", yPos: e.pageY + "px", showMenu: true});
    }
    render() {
      const { showMenu, xPos, yPos } = this.state;
          if (showMenu) {
            return (
              <div className={styles.context_menu_area} style={{top: yPos, left:xPos, height: 'auto', width: 'auto'}}>
                {this.props.menu}
              </div>
            );
          } else { return null;
        }
    }
}
ContextMenu.propTypes = {
  menu: PropTypes.object.isRequired,
}
ContextMenu.defaultProps = {
  menu: <div></div>
};

export { ContextMenu };
