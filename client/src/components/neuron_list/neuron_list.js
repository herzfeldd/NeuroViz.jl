import React from 'react';
import styles from './neuron_list.module.css'; // Must be named *.module
import { ContextMenu } from '../context_menu/context_menu.js'

const useSortableData = (items, config = null, sort_updated_callback=null) => {
  const [sortConfig, setSortConfig] = React.useState(config);

  const sortedItems = React.useMemo(() => {
    let sortableItems = [...items];
    if (sortConfig !== null) {
      sortableItems.sort((a, b) => {
        if (a[sortConfig.key] < b[sortConfig.key]) {
          return sortConfig.direction === 'ascending' ? -1 : 1;
        }
        if (a[sortConfig.key] > b[sortConfig.key]) {
          return sortConfig.direction === 'ascending' ? 1 : -1;
        }
        return 0;
      });
    }
    return sortableItems;
  }, [items, sortConfig]);

  const requestSort = (key) => {
    let direction = 'ascending';
    if (
      sortConfig &&
      sortConfig.key === key &&
      sortConfig.direction === 'ascending'
    ) {
      direction = 'descending';
    }
    setSortConfig({ key, direction });
  };
  if (sort_updated_callback !== null) sort_updated_callback(sortedItems.map((value, index) => { return value.id; }));
  return { items: sortedItems, requestSort, sortConfig };
};

function Neuron(props) {
  let current_style = styles.row;
  if (props.primary_selected) {
    current_style = styles.primary_selected;
  }
  if (props.secondary_selected) {
    current_style = styles.secondary_selected;
  }
  if (props.marked) {
    current_style = current_style + " " + styles.marked;
  }

  return (
    <tr className={current_style}  onClick={props.onClick} onContextMenu={props.onContextMenu}>
      <td>{ props.id }</td>
      <td>{ Number(props.mua).toFixed(2) }</td>
      <td>{ Number(props.snr).toFixed(2) }</td>
      <td>{ Number(props.peak_amplitude).toFixed(1) }</td>
      <td>{ props.channels.join(",") }</td>
      <td>{ Number(props.mean_fr).toFixed(1) }</td>
      <td>{ Number(props.cv2).toFixed(2) }</td>
      <td>{ props.filename}</td>
      <td>{ (props.label === null || props.label === undefined) ? "Unlabeled" : props.label }</td>
      <td>{ (props.layer === null || props.layer === undefined) ? "Unknown" : props.layer }</td>
      <td>{ props.class_type }</td>
    </tr>
  );
}

const NeuronMenu = () => (
  <ul className="menu">
    <li>TODO1</li>
    <li>TODO2</li>
  </ul>
);

function NeuronList(props)
{
  const ref = React.useRef(0)
  const context_menu_ref = React.useRef(0)
  React.useEffect(() => {
      const dom_element = ref.current
      dom_element.addEventListener("keydown", props.handle_key)
      return () => {dom_element.removeEventListener("keydown", props.handle_key)}
    })

  const handle_context_menu = (e, id, index) => {
    e.preventDefault()
    console.log(e)
    context_menu_ref.current.handleContextMenu(e)
  }

  // Add an index to each item in props
  for (let i = 0; i < props.neurons.length; i++) {
    props.neurons[i].index = i;
  }

  const { items, requestSort, sortConfig } = useSortableData(props.neurons, null, props.sort_updated);

  // Given a name, ask whether we sorted by that key. If no we return undefined
  // if yes, we return the a string that is the sort direction (e.g., "ascending" or "descending")
  const getClassName = (name) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig.key === name ? styles.[sortConfig.direction] : undefined;
  };

  return (
    <div style={{width:'100%', overflowY: 'auto', overflowX: 'auto', display: 'block'}} tabIndex="0" ref={ref} name="neuron-list-div">
      <ContextMenu menu={<NeuronMenu/>} ref={context_menu_ref} />
      <table className={styles.table} >
      <tbody>
          <tr className={styles.row} >
            <th><button onClick={() => requestSort('index')} className={getClassName('index')}>Id</button></th>
            <th><button onClick={() => requestSort('fraction_mua')} className={getClassName('fraction_mua')}>MUA</button></th>
            <th><button onClick={() => requestSort('snr', null)} className={getClassName('snr')}>SNR</button></th>
            <th><button onClick={() => requestSort('peak_amplitude', null)} className={getClassName('peak_amplitude')}>Peak Amp.</button></th>
            <th><button onClick={() => requestSort('channel')} className={getClassName('channel')}>Chan.</button></th>
            <th><button onClick={() => requestSort('mean_firing_rate')} className={getClassName('mean_firing_rate')}>Mean FR.</button></th>
            <th><button onClick={() => requestSort('cv2')} className={getClassName('cv2')}>CV2</button></th>
            <th><button onClick={() => requestSort('filename')} className={getClassName('filename')}>Filename</button></th>
            <th><button onClick={() => requestSort('label')} className={getClassName('label')}>Label</button></th>
            <th><button onClick={() => requestSort('layer')} className={getClassName('layer')}>Layer</button></th>
            <th>Type</th>
          </tr>
          {items.map((value, index) => {
            return <Neuron key={value.id} id={value.index} mua={value.fraction_mua} snr={value.snr} cv2={value.cv2} 
		peak_amplitude={value.peak_amplitude} channels={value.channels} mean_fr={value.mean_firing_rate} 
		filename={value.filename} label={value.label} layer={value.layer}
              primary_selected={props.primary_selected_id === value.id} secondary_selected={props.secondary_selected_id === value.id} class_type={value.class_type} marked={props.mark_list.includes(value.id)}
              onClick={(e) => {props.onClick(e, value.id, index); }} onContextMenu={(e) => handle_context_menu(e, value.id, index) }/>
          })}
        </tbody>
      </table>
    </div>
  );
}

export { NeuronList };
