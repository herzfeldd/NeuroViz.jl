import React from 'react';
import styles from '../neuron_list/neuron_list.module.css'; // Must be named *.module


function NeuronSimilarity(props) {
  let current_style = styles.row;

  if (props.secondary_selected) {
    current_style = styles.secondary_selected;
  }
  if (props.similarity_score > 0.50 && props.similarity_score < 0.75) {
    current_style = current_style + " " + styles.subtle_highlight;
  } else if (props.similarity_score >= 0.75) {
    current_style = current_style + " " + styles.highlight;
  }
  if (props.primary_selected) {
    return (null); // Skip the primary selected component
  }
  return (
    <tr className={current_style}  onClick={props.onClick} onContextMenu={props.onContextMenu}>
      <td>{ props.id }</td>
      <td>{ Number(props.similarity_score).toFixed(2) }</td>
    </tr>
  );
}

const useSortableData = (items, config = null, sort_updated_callback=null) => {
  const [sortConfig, setSortConfig] = React.useState(config);

  const sortedItems = React.useMemo(() => {
    let sortableItems = [...items];
    if (sortConfig !== null) {
      sortableItems.sort((a, b) => {
        if (a[sortConfig.key] < b[sortConfig.key]) {
          return sortConfig.direction === 'ascending' ? -1 : 1;
        }
        if (a[sortConfig.key] > b[sortConfig.key]) {
          return sortConfig.direction === 'ascending' ? 1 : -1;
        }
        return 0;
      });
    }
    return sortableItems;
  }, [items, sortConfig]);

  const requestSort = (key) => {
    let direction = 'descending';
    if (
      sortConfig &&
      sortConfig.key === key &&
      sortConfig.direction === 'descending'
    ) {
      direction = 'ascending';
    }
    setSortConfig({ key, direction });
  };
  if (sort_updated_callback !== null) sort_updated_callback(sortedItems.map((value, index) => { return value.id; }));
  return { items: sortedItems, requestSort, sortConfig };
};

function NeuronSimilarityList(props)
{

  // Construct a list that we will pass to our underlying elements function. Needs
  // to have an index and a similiarity_score element
  var table_values = []

  // Determine if anything is selected (and which index it is)
  let primary_selected_index = null;
  var i;
  for (i = 0; i < props.neurons.length; i++) {
    if (props.neurons[i].id === props.primary_selected_id) {
      primary_selected_index = i;
      break;
    }
  }

  if (primary_selected_index !== null && props.similarity_scores.length === props.neurons.length) {
    for (i = 0; i < props.neurons.length; i++)
    {
      table_values.push({'index': i, 'id': props.neurons[i].id, 'similarity_score': props.similarity_scores[i][primary_selected_index]})
    }
  }

  // Given a name, ask whether we sorted by that key. If no we return undefined
  // if yes, we return the a string that is the sort direction (e.g., "ascending" or "descending")
  const getClassName = (name) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig.key === name ? styles.[sortConfig.direction] : undefined;
  };

  const { items, requestSort, sortConfig } = useSortableData(table_values, null, null);

  // Set default sort
  if (sortConfig === null) {
    requestSort('similarity_score')
  }

  var neuron_similarity_list = []
  for (let index = 0; index < items.length; index++) {
    neuron_similarity_list.push(<NeuronSimilarity key={items[index].id} id={items[index].index} similarity_score={items[index].similarity_score}
      primary_selected={props.primary_selected_id === items[index].id} secondary_selected={props.secondary_selected_id === items[index].id}
      onClick={(e) => {document.getElementsByName("neuron-list-div")[0].focus(); props.onClick(e, items[index].id, items[index].index)}} onContextMenu={(e) => props.onContextMenu(e, items[index].id, items[index].index)}/>)
  }

  return (
    <div style={{width:'100%', overflowY: 'auto', overflowX: 'auto', display: 'block'}}>
      <table className={styles.table} >
      <tbody>
          <tr className={styles.row}>
            <th><button onClick={() => requestSort('index')} className={getClassName('index')}>Id</button></th>
            <th><button onClick={() => requestSort('similarity_score')} className={getClassName('similarity_score')}>Similarity</button></th>
          </tr>
          {neuron_similarity_list}
        </tbody>
      </table>
    </div>
  );
}

export { NeuronSimilarityList };
