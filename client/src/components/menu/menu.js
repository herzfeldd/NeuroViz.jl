import React, {useContext} from 'react';
import PropTypes from 'prop-types';
import './menu.css';
import Menu, { SubMenu, MenuItem } from 'rc-menu';
import { StatusContext } from '../../context.js'

function NeuroVizMenuBar(props) {
  const neuron_file_selector = React.useRef(null);
  const pl2_recording_file_selector = React.useRef(null);
  const neuron_file_append_selector = React.useRef(null);
  const status = useContext(StatusContext);
  function handle_neuron_file_select() {
    neuron_file_selector.current.value = null;
    neuron_file_selector.current.click();
  };
  function handle_pl2_recording_file_select() {
    pl2_recording_file_selector.current.value = null;
    pl2_recording_file_selector.current.click();
  };
  function handle_neuron_append_select() {
    neuron_file_append_selector.current.value = null;
    neuron_file_append_selector.current.click();
  }

  return (
    <div className="neuro-vis">
      <div className="neuro-vis-menu-bar">
        <input type="file" hidden className="neuron-file-selection" accept=".jld2,.mat,.pkl" onChange={ (e) => props.neuronFileChange(e.target.files, false) } ref={neuron_file_selector} />
        <input type="file" hidden className="neuron-file-append" accept=".jld2,.mat,.pkl" onChange={ (e) => props.neuronFileChange(e.target.files, true) } ref={neuron_file_append_selector} />
        <input type="file" hidden className="pl2-recording-file-selection" accept=".pl2" onChange={ (e) => props.localRecordingFileChange(e.target.files, "pl2") } ref={pl2_recording_file_selector} />
        <Menu mode="horizontal">
          <SubMenu title="File" key="file">
            <MenuItem key="change-server" onClick={ () => alert("TODO") }>Change Server</MenuItem>
            <SubMenu title="Open Recording" key="open-recording">
              <SubMenu title="Local" key="open-local-recording">
                <MenuItem key="open-pl2-recording-file" onClick={ handle_pl2_recording_file_select }>PL2Recording</MenuItem>
                <MenuItem key="open-flat-recording-file" onClick={ props.handleFlatFileClicked }>FlatRecording</MenuItem>
              </SubMenu>
              <SubMenu title="Remote" key="open-sever-recording">
                <MenuItem key="open-pl2-server-recording-file" onClick={ props.handleServerPL2FileClicked }>PL2Recording</MenuItem>
                <MenuItem key="open-flat-server-recording-file" onClick={ props.handleServerFlatFileClicked }>FlatRecording</MenuItem>
              </SubMenu>
            </SubMenu>
            <MenuItem key="open-neuron-file" disabled={status.recording_filename === null} onClick={ handle_neuron_file_select } >Open Neuron File</MenuItem>
            <MenuItem key="append-neuron-file" disabled={status.recording_filename === null || status.neuron_filename === null} onClick={ handle_neuron_append_select } >Append Neurons</MenuItem>
            <SubMenu title="Save"  key="save">
              <MenuItem key="save-jld2" disabled={status.neuron_filename === null} onClick={ () => props.handle_save("jld2") }>Save Julia (JLD2)</MenuItem>
              <MenuItem key="save-py" disabled={status.neuron_filename === null} onClick={ () => props.handle_save("pkl") }>Save Python (PKL)</MenuItem>
              <MenuItem key="save-mat" disabled={status.neuron_filename === null} onClick={ () => props.handle_save("mat") }>Save Matlab (MAT)</MenuItem>
            </SubMenu >
          </SubMenu>
          <SubMenu title="Edit" key="edit">
            <MenuItem key="edit-merge" disabled={status.primary_selected_id === null || status.secondary_selected_id === null} onClick={ props.handle_merge } >Merge</MenuItem>
            <MenuItem key="edit-split" disabled={status.primary_selected_id === null} onClick={ props.handlePCSplit } >Split PC</MenuItem>
            <MenuItem key="edit-remove-isi-violations" disabled={status.primary_selected_id === null} onClick={ props.remove_isi_violations }>Remove ISI Violations</MenuItem>
            <MenuItem key="edit-remove-cs-isi-violations" disabled={status.primary_selected_id === null} onClick={ props.remove_cs_isi_violations }>Remove CS ISI Violations</MenuItem>
            <MenuItem key="edit-realign" disabled={status.primary_selected_id === null} onClick={ props.realign }>Realign</MenuItem>
            <MenuItem key="edit-findall" disabled={status.primary_selected_id === null} onClick={ props.findall }>Find similar spikes</MenuItem>
            <SubMenu title="Convert" key="convert">
              <MenuItem key="edit-convert-pc" disabled={status.primary_selected_id === null || status.secondary_selected_id !== null} onClick={ () => props.convert_neuron_type("pc") }>Convert to PC</MenuItem>
              <MenuItem key="edit-convert-cs" disabled={status.primary_selected_id === null || status.secondary_selected_id !== null} onClick={ () => props.convert_neuron_type("cs") }>Convert to CS</MenuItem>
              <MenuItem key="edit-convert-normal" disabled={status.primary_selected_id === null || status.secondary_selected_id !== null} onClick={ () => props.convert_neuron_type("") }>Convert to Standard</MenuItem>
            </SubMenu>
            <SubMenu title="Label" key="label">
              <MenuItem key="edit-label-pc" disabled={status.primary_selected_id === null} onClick={ () => props.label_neuron("putative_pc") }>Put. PC</MenuItem>
              <MenuItem key="edit-label-basket" disabled={status.primary_selected_id === null} onClick={ () => props.label_neuron("putative_basket") }>Put. Basket (MLI)</MenuItem>
              <MenuItem key="edit-label-stellate" disabled={status.primary_selected_id === null} onClick={ () => props.label_neuron("putative_stellate") }>Put. Stellate (MLI)</MenuItem>
              <MenuItem key="edit-label-golgi" disabled={status.primary_selected_id === null} onClick={ () => props.label_neuron("putative_golgi") }>Put. Golgi</MenuItem>
              <MenuItem key="edit-label-mf" disabled={status.primary_selected_id === null} onClick={ () => props.label_neuron("putative_mf") }>Put. MF</MenuItem>
              <MenuItem key="edit-label-ubc" disabled={status.primary_selected_id === null} onClick={ () => props.label_neuron("putative_ubc") }>Put. UBC</MenuItem>
              <MenuItem key="edit-label-granule" disabled={status.primary_selected_id === null} onClick={ () => props.label_neuron("putative_granule") }>Put. Granule</MenuItem>
              <MenuItem key="edit-label-junk" disabled={status.primary_selected_id === null} onClick={ () => props.label_neuron("junk") }>Junk (Ctrl+j)</MenuItem>
              <MenuItem key="edit-label-unknown" disabled={status.primary_selected_id === null} onClick={ () => props.label_neuron("unknown") }>Unknown</MenuItem>
              <MenuItem key="edit-label-unlabeled" disabled={status.primary_selected_id === null} onClick={ () => props.label_neuron("unlabeled") }>Unlabeled</MenuItem>
            </SubMenu>
            <SubMenu title="Layer" key="layer">
              <MenuItem key="edit-layer-granule" disabled={status.primary_selected_id === null} onClick={ () => props.set_neuron_layer("granule_layer") }>Granule Layer</MenuItem>
              <MenuItem key="edit-layer-PC" disabled={status.primary_selected_id === null} onClick={ () => props.set_neuron_layer("pc_layer") }>PC Layer</MenuItem>
              <MenuItem key="edit-layer-molecular" disabled={status.primary_selected_id === null} onClick={ () => props.set_neuron_layer("molecular_layer") }>Molecular Layer</MenuItem>
            </SubMenu>
          </SubMenu>
          <SubMenu title="View" key="view">
            <MenuItem key="view-notes" onClick={ () => props.add_view("RecordingNotes") }>Recording Notes</MenuItem>
            <MenuItem key="view-acg" onClick={ () => props.add_view("ACG") }>ACG</MenuItem>
            <MenuItem key="view-acg-vs-rate" onClick={ () => props.add_view("ACGVsRate") }>ACG vs. Rate</MenuItem>
            <MenuItem key="view-ccg" onClick={ () => props.add_view("CCG") }>CCG</MenuItem>
            <MenuItem key="view-isi" onClick={ () => props.add_view("ISI") }>ISI</MenuItem>
            <MenuItem key="view-rate-vs-time" onClick={ () => props.add_view("RateTime") }>Rate vs. Time</MenuItem>
            <MenuItem key="view-joint-acg" onClick={ () => props.add_view("MergedACG") }>Joint ACG</MenuItem>
            <MenuItem key="view-template" onClick={ () => props.add_view("Template") }>Template</MenuItem>
            <MenuItem key="view-voltage" onClick={ () => props.add_view("VoltagePlot") }>Channel Voltages</MenuItem>
            <MenuItem key="view-template-pca-projection" onClick={ () => props.add_view("PCATemplateProjection") }>PCA Template Projection</MenuItem>
            <MenuItem key="view-template-umap-projection" onClick={ () => props.add_view("UMAPTemplateProjection") }>UMAP Template Projection</MenuItem>
          </SubMenu>
        </Menu>
      </div>
    </div>
  );
}

NeuroVizMenuBar.propTypes = {
  localRecordingFileChange: PropTypes.func.isRequired,
  handleFlatFileClicked: PropTypes.func.isRequired,
  handleServerFlatFileClicked: PropTypes.func.isRequired,
  handleServerPL2FileClicked: PropTypes.func.isRequired,
  handlePCSplit: PropTypes.func.isRequired,
  label_neuron: PropTypes.func.isRequired,
  set_neuron_layer: PropTypes.func.isRequired,
  remove_isi_violations: PropTypes.func.isRequired,
  remove_cs_isi_violations: PropTypes.func.isRequired,
  realign: PropTypes.func.isRequired,
  findall: PropTypes.func.isRequired
}

export { NeuroVizMenuBar };
