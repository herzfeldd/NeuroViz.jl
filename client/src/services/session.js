import http from "./http_common.js";
import { ServerSettings } from './global.js';
import { check_if_task } from './events.js';

const new_session = (callback, callback_on_error) => {
  return http.post("/session")
    .then((response) => {
        ServerSettings.session_id = response.data.SessionId;
        
        ServerSettings.event_source = new EventSource(ServerSettings.base_url + "/events/" + ServerSettings.session_id)
        if (callback !== undefined) callback(ServerSettings.session_id);
    }, (error) => {
        if (callback_on_error !== undefined) callback_on_error(error);
    });
};

const delete_session = () => {
  return http.delete("/session")
    .then((response) => {
      // Do nothing
    }, (error) => {
        console.log("FAILED TO DELETE")
        console.log(error);
    });
}

const get_session = (callback, error_callback) => {
  return http.get("/session")
    .then((response) => {
      if (callback !== undefined) callback(response.data);
    }, (error) => {
      if (error_callback !== undefined) error_callback(error)
    });
}

const set_neuron_file = (file, filename, callback, error_callback, progress_callback, append=false) => {
  let params = {append: append}
  const form = new FormData()
  form.append('content', file)
  return http.post("/session/neuron_file", form, {'content-type': 'multipart/form-data', 'boundary': form._boundary, params: params, onUploadProgress: (e) => {progress_callback(e)}})
    .then((response) => {
        if (callback !== undefined) callback(response.data);
    }, (error) => {
        if (error_callback !== undefined) error_callback(error);
    });
}

const set_recording_file = (file, filename, pre_task_callback, post_task_callback, error_callback, progress_callback, type=undefined, num_channels=undefined, sampling_rate=undefined, interleaved=undefined, mmap=undefined) => {
  let params = {type: type, num_channels: num_channels, sampling_rate: sampling_rate, interleaved: interleaved, mmap: mmap}
  return http.post("/session/recording_file", file, {headers: {"Content-Type": "application/octet-stream", "Content-Disposition": "attachment; filename=\"" + filename + "\""}, params: params, onUploadProgress: (e) => {progress_callback(e)}})
    .then((response) => {
      check_if_task(response, pre_task_callback, post_task_callback)
    }, (error) => {
      if (error_callback !== undefined) error_callback(error)
    });
}

const set_server_recording_file = (filename, pre_task_callback, post_task_callback, error_callback, type=undefined, num_channels=undefined, sampling_rate=undefined, interleaved=undefined, mmap=undefined) => {
  let params = {type: type, num_channels: num_channels, sampling_rate: sampling_rate, interleaved: interleaved, mmap: mmap}
  return http.post("/session/server_recording_file", {path: filename, type: type, sampling_rate: sampling_rate, num_channels: num_channels}, {params: params})
    .then((response) => {
      check_if_task(response, pre_task_callback, post_task_callback)
    }, (error) => {
      if (error_callback !== undefined) error_callback(error)
    });
}

const get_recording_file = (callback, error_callback) => {
  return http.get("/session/recording_file")
    .then((response) => {
      if (callback !== undefined) callback(response.data);
    }, (error) => {
      if (error_callback !== undefined) error_callback(error)
    });
}

const save_session = (filename=null, type="jld2", callback=undefined) => {
  return http.get("/session/neuron_file", {responseType: "blob", timeout: 30000, headers: {"Content-Type": "application/octet-stream"}, params: {type: type}})
    .then((response) => {
        console.log(response.headers)
        if (filename === null) {
          let headerLine = response.headers['content-disposition']
          let startFileNameIndex = headerLine.indexOf('"') + 1
          let endFileNameIndex = headerLine.lastIndexOf('"')
          filename = headerLine.substring(startFileNameIndex, endFileNameIndex)
        }
        filename = filename.substr(0, filename.lastIndexOf(".")) + "." + type;
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', filename); //or any other extension
        document.body.appendChild(link);
        link.click();
        if (callback !== undefined) callback(response.data);
    }, (error) => {
        console.log(error);
    });
}

/**
 * Queries the spike indices for a neuron
 */
const get_session_notes = (callback=undefined) => {
  return http.get("/session/notes")
    .then((response) => {
        if (callback !== undefined) callback(response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

export {
  new_session,
  get_session,
  delete_session,
  set_neuron_file,
  save_session,
  set_recording_file,
  set_server_recording_file,
  get_recording_file,
  get_session_notes
};
