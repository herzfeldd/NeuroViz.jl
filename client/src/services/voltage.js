import http from "./http_common.js";

/**
 * Queries the voltage across channels
 */
const get_voltage_across_channels = (callback=undefined, start_time=undefined, end_time=undefined, channels=undefined) => {
  let params = {start_time: start_time, end_time: end_time, channels: channels}
  return http.get("/voltage", {params: params})
    .then((response) => {
        if (callback !== undefined) callback(response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

export {
  get_voltage_across_channels
}
