import http from "./http_common.js";
import { check_if_task } from './events.js';

/**
 * Queries the list of available neurons
 * Takes no arguments.
 */
const get_neurons = (callback) => {
  return http.get("/neuron")
    .then((response) => {
        if (callback !== undefined) callback(response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

/**
 * Queries a similirity matrix
 * Takes no arguments.
 */
const get_neuron_similarity_matrix = (callback) => {
  return http.get("/similarity-matrix")
    .then((response) => {
        if (callback !== undefined) callback(response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

/**
 * Queries the ACG for a neuron with the given hash
 */
const get_neuron_acg = (hash, callback) => {
  return http.get("/neuron/" + hash + "/acg")
    .then((response) => {
        if (callback !== undefined) callback(hash, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

/**
 * Queries the ACG for a neuron with the given hash
 */
const get_neuron_acg_vs_rate = (hash, callback) => {
  return http.get("/neuron/" + hash + "/acg_vs_rate")
    .then((response) => {
        if (callback !== undefined) callback(hash, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

/**
 * Queries the ISI distribution for a neuron with the given hash
 */
const get_neuron_isi_distribution = (hash, callback) => {
  return http.get("/neuron/" + hash + "/isi")
    .then((response) => {
        if (callback !== undefined) callback(hash, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

/**
 * Queries the rate vs time for a neuron with the given hash
 */
const get_neuron_rate_vs_time = (hash, callback) => {
  return http.get("/neuron/" + hash + "/rate")
    .then((response) => {
        if (callback !== undefined) callback(hash, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

/**
 * Queries the CCG of neuron_2 contingent on the psikes of neuron 1
 */
const get_neuron_ccg = (hash_1, hash_2, callback) => {
  return http.get("/neuron/" + hash_2 + "/ccg/" + hash_1)
    .then((response) => {
        if (callback !== undefined) callback(hash_1, hash_2, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

/**
 * Queries the joint ACG of neuron_2 contingent on the psikes of neuron 1
 */
const get_neuron_merged_acg = (hash_1, hash_2, callback) => {
  return http.get("/neuron/" + hash_1 + "/acg/" + hash_2)
    .then((response) => {
        if (callback !== undefined) callback(hash_1, hash_2, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

/**
 * Queries the joint ISI distribution of neuron 1 + neuron_2
 */
const get_neuron_merged_isi_distribution = (hash_1, hash_2, callback) => {
  return http.get("/neuron/" + hash_1 + "/isi/" + hash_2)
    .then((response) => {
        if (callback !== undefined) callback(hash_1, hash_2, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

/**
 * Deletes a neuron with a given hash
 */
const delete_neuron = (hash, callback) => {
  return http.delete("/neuron/" + hash)
    .then((response) => {
        if (callback !== undefined) callback(hash, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

/**
 * Merge two neurons together
 */
const merge_neurons = (hash_1, hash_2, callback) => {
  http.post("/neuron/" + hash_1 + "/combine", {ids: [hash_2]})
    .then((response) => {
        if (callback !== undefined) callback(hash_1, hash_2, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
}

/**
 * Split a PC into its constitute neurons (Neuron and ComplexSpikes)
 */
const split_purkinje_cell = (hash_1, callback) => {
  http.post("/neuron/" + hash_1 + "/split")
    .then((response) => {
        if (callback !== undefined) callback(hash_1, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
}

/**
 * Queries the template for a neuron with the given hash
 */
const get_neuron_template = (hash, callback) => {
  return http.get("/neuron/" + hash + "/template")
    .then((response) => {
        if (callback !== undefined) callback(hash, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

const convert_neuron = (hash, callback=undefined, type="pc") => {
  return http.post("/neuron/" + hash + "/convert?type=" + type)
    .then((response) => {
        if (callback !== undefined) callback(hash, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

const realign = (hash, callback=undefined) => {
  return http.post("/neuron/" + hash + "/realign")
    .then((response) => {
        if (callback !== undefined) callback(response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

const findall = (hash, pre_callback=undefined, post_callback=undefined) => {
  return http.post("/neuron/" + hash + "/find-all")
    .then((response) => {
        check_if_task(response, pre_callback, post_callback)
    }, (error) => {
        console.log(error);
    });
};

/**
 * Set attribute (dictionary values) for a  neuron. Takes a dictionary of
 * attributes
 */
const set_neuron_attributes = (hash, attributes, callback=undefined) => {
  return http.post("/neuron/" + hash + "/attributes", attributes)
    .then((response) => {
        if (callback !== undefined) callback(hash, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};


/**
 * Queries the spike indices for a neuron
 */
const get_neuron_spiketimes = (hash, callback=undefined, start_time=undefined, end_time=undefined) => {
  let params = {start_time: start_time, end_time: end_time}
  return http.get("/neuron/" + hash + "/spiketimes", {params: params})
    .then((response) => {
        if (callback !== undefined) callback(hash, response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
};

/**
 * Queries the projection of templates into 2D space for a neuron with the given hash
 */
const get_template_projection = (hash, pre_callback=undefined, post_callback=undefined, type="pca", second_unit_id=null) => {
  let params = {type: type}
  if (second_unit_id !== null) params["second_unit_id"] = second_unit_id 
  return http.get("/neuron/" + hash + "/template-projection", {params: params})
    .then((response) => {
      check_if_task(response, pre_callback, (data) => post_callback(hash, data))
    }, (error) => {
        console.log(error);
    });
};

/**
 * Modify neuron
 */
const modify_neuron = (hash, spike_indices, type, callback) => {
  http.post("/neuron/" + hash + "/modify", {spikes: spike_indices, type: type})
    .then((response) => {
        if (callback !== undefined) callback(response.data); // Call back with hash and response as args
    }, (error) => {
        console.log(error);
    });
}

export { get_neurons,
         get_neuron_acg,
         get_neuron_acg_vs_rate,
         get_neuron_isi_distribution,
         get_neuron_rate_vs_time,
         get_neuron_ccg,
         get_neuron_merged_acg,
         get_neuron_merged_isi_distribution,
         delete_neuron,
         findall,
         realign,
         merge_neurons,
         get_neuron_template,
         set_neuron_attributes,
         convert_neuron,
         split_purkinje_cell,
         get_neuron_spiketimes,
         get_neuron_similarity_matrix,
         get_template_projection,
         modify_neuron
       };
