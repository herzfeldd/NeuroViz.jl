/* Defines global variables that can be imported into services and components */
/* These should be used sparingly */

/**
 * Define a global variable that determines the server parameters for all calls.
 * By default we look for a server running on localhost 3000 and the
 * session id is unset.
 */
export const ServerSettings  = {base_url: 'http://localhost:9123/api/neuroviz/v1', session_id: "", event_source: null};
