import axios from 'axios';
import { ServerSettings } from './global.js';

// Our default instance
const instance = axios.create({
  baseURL: ServerSettings.base_url,
  headers: {
    "Content-type": "application/json",
    "Access-Control-Expose-Headers": "*",
    "Access-Control-Allow-Headers": "*",
  }
});

// axios request interceptor runs before every request, allowing us to
// inject the session id if it hasn't been specified (if the session id is
// updated between requests, this ensures that future requests always use
// the latest value.
instance.interceptors.request.use(
  (config) => {
    // Before request is sent, update our session id
    if (! config.headers.SessionId) {
      config.headers.SessionId = ServerSettings.session_id;
    }
    return config;
  },
  error => Promise.reject(error)
);

export default instance;
