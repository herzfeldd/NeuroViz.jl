import http from "./http_common.js";
import { ServerSettings } from './global.js';

/**
 * Deletes a task with a given id
 */
export const delete_task = (task_id, callback) => {
  return http.delete("/task/" + task_id)
    .then((response) => {
        if (callback !== undefined) callback(response.data);
    }, (error) => {
        console.log(error);
    });
};


/**
 * Gets the results of a task with a given hash
 */
export const get_task = (task_id, callback, args) => {
  return http.get("/task/" + task_id)
    .then((response) => {
      if (response.data.status !== null && response.data.status !== undefined && response.data.status === "waiting") {
        console.log("Re-running call")
        return get_task(task_id, callback, args); // Something went wrong and task is not finished yet. Re-run the call
      }
      if (callback !== undefined) callback(response.data, args);
    }, (error) => {
        console.log(error);
    });
};

// Check if the response contained a task. If it did, then we wait for an
// event to fire with the id associated with the task (by specifying once, this
// listener is removed after the event is received). Given that there can be some
// time between receiving the response, a "pre" call back can be provided which will
// be called only if we have to wait for the results. Otherwise, we call the "post"
// callback
export const check_if_task = (response, pre_task_callback, post_task_callback, args) => {
  if (response.data.task_id !== undefined && response.data.task_id !== null) {
    console.log("Waiting for task with id " + response.data.task_id)
    // Use this task id to wait for an event with this id and then run
    // the associated callback again
    ServerSettings.event_source.addEventListener(response.data.task_id, (e) => {console.log(e); get_task(response.data.task_id, post_task_callback, args)}, {once: true})
    if (pre_task_callback !== undefined) pre_task_callback(args)
  } else {
    if (post_task_callback !== undefined) post_task_callback(response.data, args);
  }
}
