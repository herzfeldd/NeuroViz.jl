import React from 'react';

const StatusContext = React.createContext({recording_filename: null, neuron_filename: null, primary_selected_id: null, secondary_selected_id: null}); // Create a context object

export {
  StatusContext
}
