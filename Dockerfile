FROM julia:1.6
ARG CI_JOB_TOKEN
ENV CI_JOB_TOKEN=$CI_JOB_TOKEN
ENV JULIA_NUM_THREADS=8
RUN apt-get update && apt-get install -y nginx supervisor python3 python3-dev python3-matplotlib
COPY client/build /var/www/html
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN julia -e 'import Pkg; Pkg.add(Pkg.PackageSpec(url=string("https://gitlab-ci-token:", ENV["CI_JOB_TOKEN"], "@gitlab.oit.duke.edu/herzfeldd/NeurophysToolbox.jl.git"), rev="master"))'
RUN julia -e 'import Pkg; Pkg.add(Pkg.PackageSpec(url=string("https://gitlab-ci-token:", ENV["CI_JOB_TOKEN"], "@gitlab.oit.duke.edu/herzfeldd/NeuroViz.jl.git"), rev="master"))'
RUN julia -e 'import Pkg; Pkg.build("NeuroViz"); import NeuroViz'

EXPOSE 80
EXPOSE 9000
CMD ["/usr/bin/supervisord"]
