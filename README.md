# NeuroViz
This package provides a rudimentary GUI for the NeurophysToolbox package.
It allows exploration and manipulation of neuron and recording objects.

## Usage
NeuroViz consists of two parts: an HTTP server that is written in Julia
(the backend) and a ReactJS client (the frontend). It is possible that the
backend and the frontend run on the same system. However, it is also possible
to configure `NeuroViz` so that the backend server runs on a separate system
from the frontend.

The frontend is designed to be very light weight so that it does not
consume any processing power. Rather, its primary purpose is merely to
display the data that the backend computes.

## General Usage
This project builds a complete Dockerized container that contains both the
Julia backend as well as the ReactJS frontend. For a standard user that wishes
to run NeuroViz on their own system, this is probably the easiest installation
method.

First, ensure that you have a reasonably new version of Docker (or the equivalent - e.g., podman)
installed on your system and accessible via the terminal.

### 1. Create a Gitlab Personal Access Token
To run this image on your local machine, first make a
personal access token to read the image registry. To create a personal access token,
click on your user icon in Gitlab (upper right hand corner) and click settings -> Access Tokens.
Create a new token (temporary password) that is allowed to `read_registry`.
Make sure you copy the password as you won't see it again. Then,
on your local machine, run in the terminal:

`docker login gitlab-registry.oit.duke.edu`

This will prompt you for your Duke Gitlab credentials. Enter your Gitlab
login name and the personal access token that you created. You only need
to perform this process once per session.

### 2. Run the image
After logging in you can run the image using the standard `docker run` commands. You can download and run the
complete image by performing the following at the terminal.

`
docker run -d --name neuroviz -p <port>:80/tcp -p 9123:9123/tcp gitlab-registry.oit.duke.edu/herzfeldd/neuroviz.jl/neuroviz:latest
`

The `<port>` should be an open port on your local system that your user is able
to bind. Typically, you can use any port greater than 1024. A safe value might be, for instance, `3000`.
In this case, the command is:

`
docker run -d --name neuroviz -p 3000:80/tcp -p 9123:9123/tcp gitlab-registry.oit.duke.edu/herzfeldd/neuroviz.jl/neuroviz:latest
`

Once the container is running, you should be able to navigate to `http://localhost:<port>` in
the browser of your choosing (in the above example, this would be `localhost:3000`).

You can stop the container by running `docker stop neuroviz`.

### 3. Updates
From time to time, it may be necessary to pull an updated image from the gitlab instance. In that case,
after running `docker login gitlab-registry.oit.duke.edu`, you can run

`
docker pull gitlab-registry.oit.duke.edu/herzfeldd/neuroviz.jl/neuroviz:latest
`

### 4. Configuration
The default docker configuration shown above creates a completely isolated NeuroViz server
on your system. In this configuration, the NeuroViz backend cannot access any "remote" files.
See below for a discussion of what this means. If you want to be able to manipulate files on
your local machine without the need to "upload" them to the backend Julia webserver, you can
mount a volume inside the docker container using the standard (`-v`) switch.

For instance, imagine that all of your neurophysiology files were located on your current
system in `/mnt/neurophys`. For simplicitiy, we will map this path to `/data` inside the
docker container. In this case, the docker run command would look like

`
docker run -d --name neuroviz -p <port>:80/tcp -p 9123:9123/tcp -v /mnt/neurophys:/data gitlab-registry.oit.duke.edu/herzfeldd/neuroviz.jl/neuroviz:latest
`

Now, all the files in `/mnt/neurophys` on your local machine will be available inside the container in
`/data`. So, if you wanted to open a local file in NeuroViz with the path `/mnt/neurophys/recording.pl2`,
you would specify the path as `/data/recording.pl2` in the "remote" menu. Note: you can have
multiple `-v` switches to mount as many local paths as you desire.

## Hints and tricks
### Neuron List Shortcuts
A brief list of short-cuts when the neuron-list panel is active:

| Key Combo      | Description |
|----------------|-------------|
| Up/down        | Moves the primary selected neuron (blue highlight) up or down |
| Delete         | Deletes the current primary (blue) unit |
| Ctrl + Up/Down | Moves the secondary selected neuron (red highlight) up or down |
| Ctrl + M       | Merges two units (selected red and blue) |
| Ctrl + C       | Converts a unit to a Neurophys.ComplexSpike |
| Ctrl + P       | Converts a unit to a Neurophys.PurkinjeCell |
| Ctrl + R       | Converts a unit to a Neurophys.Neuron (standard unit) |
| Ctrl + S       | Export the current session as  JLD2 file |
| Ctrl + 1	 | Mark a neuron (highlight it in the list for future reference) |
| Left           | Advance voltage plots by 1/10th of a display |
| Right          | Move voltage plots back in time by 1/10 of a display |

Note that the neurons can be sorted by the appropriate column when clicking on the
column header.

### Channel voltage shortcuts
A brief list of mouse/keyboard short-cuts when hovering over the channel voltage plots:

| Key Combo      | Description |
|----------------|-------------|
| Scroll         | Scroll across channels |
| Ctrl + Scroll  | Increase/decrease the voltage scaling (vertical axis) of each channel |
| Alt + Scroll   | Increase/decrease the amount of time (horizontal axis) shown |
| Shift + Scroll | Increase/decrease the number of channels displayed simultaneously |

### PCA/UMAP hints and tricks
While on the PCA screen you can use the scroll key to zoom as well as click + drag
to move around the space. To highlight points, hold the Shift key and drag around the
points you would like to select. Once points are selected either press Delete to remove
the points from the current neuron or press the `s` key to split these clips into
a separate neuron.

### Drag & Drop Hints
You can drag and drop both voltage files (`*.pl2`, `*.dat`, `*.bin`) files
onto the screen and neuroviz will use that file as the local recording.

You can drag and drop neuron files (appropriate `*.jld2`, `*.mat`, or `*.pkl`) files
onto the screen to use that file as the current neuron file. If you hold `Ctrl` while
dropping a neuron file, it will "append" those neurons to the current list.

## Developers' Installation Guide
Development likely happens with the backend and the frontend running on the
same machine.

First, checkout the contents of this respository in a new directory.

### Backend Installation
Navigate to the root of the checked out folder (`NeuroViz.jl`) and install
the server. This will involve downloading a relatively recent version of
Julia (v1.3 or higher). Start Julia from within the downloaded directory (`NeuroViz.jl`)
and then type:
```julia
using Pkg
Pkg.develop(Pkg.PackageSpec(path="."))
```
This should install all of the necessary Julia dependencies. It is important
to note that this package depends on the `NeurophysToolbox` package that
is not currently available in standard repositories. If you have access to
this project, you should also have access to the `NeurophysToolbox`
respository - which should be installed first.

After successfully installing the server using the commands above, you should
be able to start the server (which, by default, runs on port 9123). In the
julia terminal run:
```julia
using NeuroViz
NeuroViz.serve()
```

### Frontend installation
First, ensure that you have a recent versin of `npm` installed on your
system. See the [npm documentation](https://www.npmjs.com/) for information
about how to install `npm`.

After installing npm, navigate to `NeuroViz.jl\client`. While in this
directory, run `npm i` to install all necessary dependencies. After this
command completes successfully, run `npm start` to launch the client.

Your web browser should automatically be directed to the client. If not,
you can try to access the client on its standard port at
[http://localhost:3000](http://localhost:3000).

### Using server "local" recordings
The NeuroViz system is build around a server-client architecture such that
the backend and the frontend can reside on different systems. Therefore, any
recording or neuron files that are analyzed in NeuroViz are transferred from
the local system where the frontend is running to the backend server. However,
it is sometimes the case that data resides on the server that we want to allow
the user to access without the need to transfer files over the network. When
starting the server, an extra flag can be passed to allow the server to load
recordings from paths local to the server's system. Multiple paths are allowed
as a vector of regular expressions. When the user requests to open a server backed
recording file, the file path is check against these allowed paths. As an
example, the following configuration allows the frontend to request any recording
files (at any path) on the remote server.
```julia
using NeuroViz
NeuroViz.serve(allowed_paths=[r".*"])
```

Alternatively, if you wanted frontend users to access server resources in the
`/mnt/data` and `/mnt/recordings` directories, the server should be started with:
```julia
using NeuroViz
NeuroViz.serve(allowed_paths=[r"/mnt/data/*", r"/mnt/recordings/*"])
```
