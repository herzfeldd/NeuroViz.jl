module NeuroViz

import JSON2
import HTTP
import Dates
import Sockets
import MultivariateStats
import UMAP
import StatsBase
import Statistics
import DSP
using NeurophysToolbox

# Define our router (GLOBAL) for all functions. API endpoints are defines in the router.
const ROUTER = HTTP.Router()
const SESSIONS = Dict{UInt128, Dict{Symbol, Any}}()
const PORT = 9123
const ALLOWED_LOCAL_PATHS = Vector{Regex}(undef, 0)

include("common.jl")
include("session.jl")
include("neuron.jl")
include("voltage.jl")
include("events.jl")

mutable struct Session
    neuron_filename::Union{Nothing, String}
    recording_filename::Union{Nothing, String}
    background_tasks::Dict{UInt128, BackgroundTask}
    task_channel::Channel
end
function Session()
    return Session(nothing, nothing, Dict{UInt128, Task}(), Channel{Tuple{UInt128, AbstractString}}(100)) # Allow 100 messages
end

"""
    serve()

Primary endpoint for the server.
"""
function serve(; port::Integer=PORT, allowed_paths::Vector{Regex}=Vector{Regex}(undef, 0))
    deleteat!(ALLOWED_LOCAL_PATHS, 1:length(ALLOWED_LOCAL_PATHS))
    for path = allowed_paths
        push!(ALLOWED_LOCAL_PATHS, path)
    end

    HTTP.serve(StreamandRequestHandler, Sockets.IPv4(0), port, reuseaddr=false, verbose=true, stream=true)
end

"""
    JSONHander(req)

Parse returned objects as JSON
"""
function JSONHandler(req::HTTP.Request)
    response_body = ROUTER(req)
    if response_body == nothing
        return CORSResponse(HTTP.Response(404, "invalid request/not found"))
    elseif isa(response_body, AbstractString) && length(response_body) > 0
        return CORSResponse(HTTP.Response(404, response_body))
    else
        if isa(response_body, HTTP.Messages.Response)
            return response_body # Nothing to do, just return it
        elseif isa(response_body, AbstractString) && length(response_body) == 0
            return CORSResponse(HTTP.Response(204))
        else
            return CORSResponse(HTTP.Response(200, JSON2.write(response_body)))
        end
    end
end

# AuthHandler to reject any unknown sesions
function AuthHandler(req::HTTP.Request)
    # Check if request is for create session only
    if req.method == "POST" && req.target == "/api/neuroviz/v1/session"
        response = JSONHandler(req)
    elseif HTTP.hasheader(req, "SessionId") && haskey(SESSIONS, get_session_id(req))
        response = JSONHandler(req)
    else
        response = CORSResponse(HTTP.Response(401, "unauthorized"))
    end
    return response
end

function CORSHandler(req::HTTP.Request)
    # TODO: This should be moved elsewhere
    clear_stale_sessions()

    if req.method == "OPTIONS" && req.target == "/api/neuroviz/v1/session/neuron_file"
        return CORSResponse(HTTP.Response(200), "application/octet-stream")
    elseif req.method == "OPTIONS"
        return CORSResponse(HTTP.Response(200))
    else
        return AuthHandler(req)
    end
end

function StreamandRequestHandler(stream::HTTP.Stream)
    request::HTTP.Request = stream.message
    if request.method == "GET" && occursin("/api/neuroviz/v1/events", request.target)
        @show request
        return ROUTER(stream)
    end
    println(request.method, " ", request.target)
    request.body = read(stream)
    HTTP.closeread(stream)
    request.response::HTTP.Response = CORSHandler(request)
    request.response.request = request
    @show request.response
    HTTP.startwrite(stream)
    write(stream, request.response.body)
end

function clear_stale_sessions()
    for k = keys(SESSIONS)
        if haskey(SESSIONS[k], :last_check_in) && ((Dates.now() - SESSIONS[k][:last_check_in]) / Dates.Millisecond(1)) > 60 * 1000 # Invalidate session after 60 seconds
            println("WARN: Deleting stale session with id $k")
            delete_session(k)
        end
    end
end

export serve


end # module
