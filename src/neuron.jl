
function _neuron_to_dict(neuron::AbstractNeuron, recording::AbstractNeurophysiologyRecording)
    params = Dict{String, Any}()
    params["num_spikes"] = length(spike_indices(neuron))
    class_type = string(typeof(neuron))
    if occursin(".", class_type)
        class_type = split(class_type, ".")[end]
    end
    params["class_type"] = class_type
    params["mean_firing_rate"] = length(spike_indices(neuron)) / (spike_times(neuron)[end] - spike_times(neuron)[1])
    params["fraction_mua"] = fraction_mua(neuron)
    params["snr"] = snr(neuron, recording, max_clips=1000)
    params["id"] = string(hash(neuron))
    params["dt"] = dt(neuron)
    params["cv"] = cv(neuron)
    params["cv2"] = cv2(neuron)
    params["peak_amplitude"] = peak_amplitude(neuron, recording, max_clips=1000)
    params["sampling_rate"] = sampling_rate(neuron)
    params["channels"] = channel_id(neuron)
    params["filename"] = haskey(neuron, :filename) ? Base.Filesystem.basename(neuron[:filename]) : "unknown"
    params["label"] = haskey(neuron, :label) ? neuron[:label] : "Unlabeled"
    params["layer"] = haskey(neuron, :layer) ? neuron[:layer] : "Unknown"
    return params
end

function get_neurons(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS[id], :neurons) || SESSIONS[id][:neurons] == nothing
        return "no valid neurons"
    end
    if ! haskey(SESSIONS[id], :recording) || SESSIONS[id][:recording] == nothing
        return "no valid recording"
    end
    d = nothing
    if haskey(SESSIONS[id], :neuron_parameters_cache) && SESSIONS[id][:neuron_parameters_cache] != nothing
        # Update our hash (if it has changed)
        d = SESSIONS[id][:neuron_parameters_cache]
        d["num_neurons"] = length(SESSIONS[id][:neurons])
        hashes = [neuron_dict["id"] for neuron_dict in d["neurons"]]
        found = falses(length(hashes))
        for neuron = SESSIONS[id][:neurons]
            I = findfirst(hashes .== string(hash(neuron)))
            if I == nothing
                # Add this neuron to our list
                push!(d["neurons"], _neuron_to_dict(neuron, SESSIONS[id][:recording]))
                push!(hashes, d["neurons"][end]["id"])
                push!(found, true)
                continue
            end
            found[I] = true
        end
        # Delete any "unfound" (previously) deleted neurons
        deleteat!(d["neurons"], found .== false)
    else
        # Create a new parameter list and cache it
        d = Dict{String, Any}()
        d["num_neurons"] = length(SESSIONS[id][:neurons])
        d["neurons"] = Vector{Dict{String, Any}}(undef, 0)
        for neuron = SESSIONS[id][:neurons]
            push!(d["neurons"], _neuron_to_dict(neuron, SESSIONS[id][:recording]))
        end
    end
    SESSIONS[id][:neuron_parameters_cache] = d

    return d
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/neuron", get_neurons)

# Shifts a single channel template by delay
function _shift_template(template::AbstractVector, delay::Integer)
    template_shifted = zeros(eltype(template), length(template))
    if delay > 0
        current_delay = delay
        template_shifted[1:length(template)-current_delay] = template[1+current_delay:end]
    else
        current_delay = delay * -1
        template_shifted[1+current_delay:end] = template[1:length(template)-current_delay]
    end
    return template_shifted
end

# Return a num_neurons x num_neurons similarity matrix (using regression rather
# than the correlation coefficient)
function get_neuron_similarity_matrix(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS[id], :neurons) || SESSIONS[id][:neurons] == nothing
        return "invalid neuron file"
    end
    # Compute a list of templates for each neuron (across all channels)
    neuron_templates = Vector{Vector{Real}}(undef, 0)
    for (i, neuron) = enumerate(SESSIONS[id][:neurons])
        template = Vector{Real}(undef, 0)
        for channel = 1:length(SESSIONS[id][:recording])
            current_channel_template = NeurophysToolbox.compute_template(neuron, SESSIONS[id][:recording], channel=channel, max_clips=1000)
            append!(template, current_channel_template)
        end
        push!(neuron_templates, template)
    end
    # Compute the similarity matrix (regression)
    similarity_matrix = zeros(length(neuron_templates), length(neuron_templates))
    for i = 1:length(neuron_templates)
        for j = i:length(neuron_templates)
            if (i == j)
                similarity_matrix[i, j] = NaN
                continue
            end
            # Find the optimal lead/lag between the two templates
            delay = argmax(DSP.xcorr(convert.(Float32, neuron_templates[i]), convert.(Float32, neuron_templates[j]), padmode=:none)) - length(neuron_templates[i])
            delay = abs(delay) > (length(neuron_templates[i]) / length(SESSIONS[id][:recording]) / 2) ? 0 : delay # Only allow templates to shift by half
            shifted_template_i = _shift_template(neuron_templates[i], delay)
            #shifted_template_j = _shift_template(neuron_templates[j], -delay)
            shifted_template_j = neuron_templates[j] # NOTE: We could shift each by half instead of shifting 1 by delay
            similarity_matrix[i, j] = regression = min(shifted_template_i \ shifted_template_j, shifted_template_j \ shifted_template_i)
            similarity_matrix[j, i] = similarity_matrix[i, j]
        end
    end

    return Dict("values" => [similarity_matrix[index, :] for index = 1:size(similarity_matrix, 1)], "channels" => 1:length(SESSIONS[id][:recording]), "ids" => [string(hash(neuron)) for neuron in SESSIONS[id][:neurons]])
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/similarity-matrix", get_neuron_similarity_matrix)

function get_neuron_acg(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS[id], :neurons) || SESSIONS[id][:neurons] == nothing
        return "invalid neuron file"
    end
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/acg, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]
    time_axis = -100e-3:1e-3:100e-3
    values = NeurophysToolbox.gen_cross_correlogram(spike_indices(neuron), dt(neuron), spike_indices(neuron), dt(neuron), time_axis=time_axis, set_zero=true)
    return Dict("values" => values, "limits" => (time_axis[1], time_axis[end]), "dt" => 1e-3, "ids" => (neuron_id, neuron_id))
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/neuron/*/acg", get_neuron_acg)

function get_neuron_acg_vs_rate(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS[id], :neurons) || SESSIONS[id][:neurons] == nothing
        return "invalid neuron file"
    end
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/acg, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]
    time_axis = -100e-3:1e-3:100e-3
    if ! haskey(neuron,  :acg_vs_rate)
        neuron[:acg_vs_rate] = NeurophysToolbox.gen_auto_correlogram_vs_firing_rate(spike_indices(neuron), dt(neuron), time_axis=time_axis)
    end
    bins, values = neuron[:acg_vs_rate]
    # Convert values into a array of arrays
    values = [values[i, :] for i = 1:size(values, 1)]

    return Dict("values" => values, "bins" => bins, "limits" => (time_axis[1], time_axis[end]), "dt" => 1e-3, "ids" => (neuron_id, neuron_id))
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/neuron/*/acg_vs_rate", get_neuron_acg_vs_rate)

function get_neuron_joint_acg(req::HTTP.Request)
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_1_id = nothing
    neuron_2_id = nothing
    try
        neuron_1_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#1/ccg/#2, get neuron #1
        neuron_2_id = parse(UInt64, paths[7]) # /api/neuroviz/v1/neuron/#1/ccg/#2, get neuron #2
    catch
        return "invalid neuron hash/unparseable"
    end
    I1 = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_1_id)
    I2 = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_2_id)
    if I1 == nothing || I2 == nothing
        return "invalid neuron hash/not found"
    end
    neuron_1 = SESSIONS[id][:neurons][I1]
    neuron_2 = SESSIONS[id][:neurons][I2]
    time_axis = -100e-3:1e-3:100e-3
    if dt(neuron_1) != dt(neuron_2)
        return "invalid dt"
    end
    # Combine spike indices
    combined_indices = unique(sort(vcat(spike_indices(neuron_1), spike_indices(neuron_2))))

    values = NeurophysToolbox.gen_cross_correlogram(combined_indices, dt(neuron_1), combined_indices, dt(neuron_1), time_axis=time_axis, set_zero=true)
    return Dict("values" => values, "limits" => (time_axis[1], time_axis[end]), "dt" => 1e-3, "ids" => (neuron_1_id, neuron_2_id))
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/neuron/*/acg/*", get_neuron_joint_acg)

function get_neuron_rate(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS[id], :neurons) || SESSIONS[id][:neurons] == nothing
        return "invalid neuron file"
    end
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/acg, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]
    total_seconds = maximum([Integer(ceil((spike_indices(x)[end] * dt(x)))) for x in SESSIONS[id][:neurons]])
    bin_size = min(total_seconds / 10, 60)
    # Split into 10 samples initially
    samples_per_bin = Integer(ceil(bin_size / dt(neuron)))

    indices = spike_indices(neuron)
    fr = zeros(Integer(floor(total_seconds / bin_size)))
    for i = 1:length(fr)
        start = (i-1) * samples_per_bin + 1
        stop = start + samples_per_bin
        fr[i] = sum((indices .>= start) .& (indices .<= stop)) / ((stop - start) * dt(neuron))
    end
    return Dict("values" => fr, "limits" => (0, total_seconds), "dt" => bin_size)
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/neuron/*/rate", get_neuron_rate)

function get_neuron_isi(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS[id], :neurons) || SESSIONS[id][:neurons] == nothing
        return "invalid neuron file"
    end
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/acg, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]
    time_axis = 0e-3:1e-3:100e-3
    values = NeurophysToolbox.gen_isi_histogram(neuron, time_axis=time_axis, normalize="probability")
    return Dict("values" => values, "limits" => (time_axis[1], time_axis[end]), "dt" => 1e-3)
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/neuron/*/isi", get_neuron_isi)

function get_neuron_template(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS[id], :neurons) || SESSIONS[id][:neurons] == nothing
        return "invalid neuron file"
    end
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/acg, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]

    uri = HTTP.URIs.URI(req.target)
    x = HTTP.queryparams(uri)
    channel_start = get(x, "channel_start", "1")
    channel_end = get(x, "channel_end", string(length(SESSIONS[id][:recording])))
    try
        channel_start = parse(UInt64, channel_start)
        channel_end = parse(UInt64, channel_end)
    catch
        return "unbale to parse channel_start/channel_stop parameter"
    end
    channel_range = channel_start:channel_end

    templates = []
    for channel = channel_range
        template = NeurophysToolbox.compute_template(neuron, SESSIONS[id][:recording], channel=channel, max_clips=1000)
        push!(templates, template)
    end
    return Dict("values" => templates, "channels" => channel_range, "dt" => dt(neuron))
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/neuron/*/template", get_neuron_template)

function get_neuron_ccg(req::HTTP.Request)
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_1_id = nothing
    neuron_2_id = nothing
    try
        neuron_1_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#1/ccg/#2, get neuron #1
        neuron_2_id = parse(UInt64, paths[7]) # /api/neuroviz/v1/neuron/#1/ccg/#2, get neuron #2
    catch
        return "invalid neuron hash/unparseable"
    end
    I1 = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_1_id)
    I2 = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_2_id)
    if I1 == nothing || I2 == nothing
        return "invalid neuron hash/not found"
    end
    neuron_1 = SESSIONS[id][:neurons][I1]
    neuron_2 = SESSIONS[id][:neurons][I2]
    time_axis = -100e-3:1e-3:100e-3
    values = NeurophysToolbox.gen_cross_correlogram(spike_indices(neuron_1), dt(neuron_1), spike_indices(neuron_2), dt(neuron_2), time_axis=time_axis)
    return Dict("values" => values, "limits" => (time_axis[1], time_axis[end]), "dt" => 1e-3, "ids" => (neuron_1_id, neuron_2_id))
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/neuron/*/ccg/*", get_neuron_ccg)

function delete_neuron(req::HTTP.Request)
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/acg, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    deleteat!(SESSIONS[id][:neurons], [hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    return Dict()
end
HTTP.register!(ROUTER, "DELETE", "/api/neuroviz/v1/neuron/*", delete_neuron)

function combine_neurons(req::HTTP.Request)
    body = IOBuffer(HTTP.payload(req))
    # TODO: Is this really a post...expects a JSON list of neurons to combine
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/combine, get neuron #
    catch e
        return string(e, "invalid neuron hash/unparseable")
    end
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron index"
    end
    neuron = SESSIONS[id][:neurons][I]

    x = nothing
    try
        x = JSON2.read(body)
    catch ex
        return return string(ex)
    end
    if ! haskey(x, :ids)
        return "invalid data - expected ids"
    end
    ids = nothing
    try
        ids = parse.(UInt64, x[:ids]) # /api/neuroviz/v1/neuron/#/combine, get neuron #
    catch e
        return string(e, "invalid list of ids/unparseable")
    end

    indices = [I]
    # MAJOR TODO
    for current_id = ids
        if id == neuron_id
            continue
        end
        I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== current_id)
        if I == nothing
            continue
        end
        push!(indices, I)
    end

    filename = SESSIONS[id][:neurons][indices[1]][:filename]
    for neuron = SESSIONS[id][:neurons][indices]
        if haskey(neuron, :filename) && neuron[:filename] == filename
            continue
        else
            filename = "merged"
        end
    end
    combine_neurons!(SESSIONS[id][:neurons], indices)
    NeurophysToolbox.align_clips!(SESSIONS[id][:neurons][end], SESSIONS[id][:recording])
    SESSIONS[id][:neurons][end][:filename] = filename

    return _neuron_to_dict(SESSIONS[id][:neurons][end], SESSIONS[id][:recording])
end
HTTP.register!(ROUTER, "POST", "/api/neuroviz/v1/neuron/*/combine", combine_neurons)


function set_neuron_attributes(req::HTTP.Request)
    body = IOBuffer(HTTP.payload(req))
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/acg, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]

    attribute_list = nothing
    try
        attribute_list = JSON2.read(body)
    catch ex
        return return string(ex)
    end
    for k = keys(attribute_list)
        neuron[Symbol(k)] = attribute_list[k]
    end
    SESSIONS[id][:neurons][I] = neuron
    SESSIONS[id][:neuron_parameters_cache] = nothing
    return Dict{String, Any}()
end
HTTP.register!(ROUTER, "POST", "/api/neuroviz/v1/neuron/*/attributes", set_neuron_attributes)

function realign_clips(req::HTTP.Request)
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/acg, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    NeurophysToolbox.align_clips!(SESSIONS[id][:neurons][I], SESSIONS[id][:recording])

    SESSIONS[id][:neuron_parameters_cache] = nothing
    return _neuron_to_dict(SESSIONS[id][:neurons][I], SESSIONS[id][:recording])
end
HTTP.register!(ROUTER, "POST", "/api/neuroviz/v1/neuron/*/realign", realign_clips)

function find_all(req::HTTP.Request)
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/acg, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]
    uri = HTTP.URIs.URI(req.target)
    x = HTTP.queryparams(uri)
    sigma = get(x, "threshold", "3.5")
    try
        sigma = parse(Float64, sigma)
    catch
        return "invalid threshold specified"
    end

    task_id, _ = run_task(id, _compute_findall_spikes, neuron; sigma=sigma)
    return Dict{String, Any}("status" => "processing", "task_id" => string(task_id))
end
function _compute_findall_spikes(session_id::UInt128, task_id::UInt128, task_channel::Channel, neuron::AbstractNeuron; sigma::Real=5)
    # Get the template for the current unit (only on the current channel)
    channel = StatsBase.mode(spike_indices_channel(neuron))
    template = NeurophysToolbox.compute_template(neuron, SESSIONS[session_id][:recording], channel=channel, max_clips=1000)
    convolved_result = zeros(length(SESSIONS[session_id][:recording][channel][SpikeTimeseries]))
    recording_channel = SESSIONS[session_id][:recording][channel][SpikeTimeseries]
    # Center on the middle of the template
    half_template = Integer(round(length(template) / 2))
    convolved_result = DSP.conv(convert.(Float32, recording_channel), reverse(template))
    convolved_result = convolved_result[half_template:length(convolved_result) - half_template]
    median_threshold = Statistics.median(abs.(recording_channel) * sigma) / 0.6745
    median_threshold = median_threshold + (sum(template.^2) - median_threshold) / 2

    # Find the peak (with a blank period equal to the length of template)
    indices = Vector{UInt64}(undef, 0)
    i = 1
    peak_value = -Inf
    peak_index = 0
    while i < length(convolved_result)
        if (convolved_result[i] > median_threshold)
            if convolved_result[i] > peak_value
                peak_index = i
                peak_value = convolved_result[i]
            end
        elseif (convolved_result[i] < median_threshold)
            push!(indices, peak_index)
            peak_value = -Inf
            i = i + length(template)
            continue
        end
        i = i + 1
    end
    new_neuron = Neuron(indices, ones(UInt64, length(indices)) .* channel, sampling_rate(recording_channel), filename=filename(neuron))
    if haskey(neuron, :filename)
        new_neuron[:filename] = neuron[:filename]
    end
    NeurophysToolbox.align_clips!(new_neuron, SESSIONS[session_id][:recording])

    push!(SESSIONS[session_id][:neurons], new_neuron)
    put!(task_channel, (task_id, "findall"))
    return _neuron_to_dict(SESSIONS[session_id][:neurons][end], SESSIONS[session_id][:recording])
end
HTTP.register!(ROUTER, "POST", "/api/neuroviz/v1/neuron/*/find-all", find_all)


function convert_neuron(req::HTTP.Request)
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/acg, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]

    uri = HTTP.URIs.URI(req.target)
    x = HTTP.queryparams(uri)
    type = get(x, "type", "")
    if type == "pc"
        SESSIONS[id][:neurons][I] = NeurophysToolbox.PurkinjeCell(neuron)
    elseif type == "cs"
        SESSIONS[id][:neurons][I] = NeurophysToolbox.ComplexSpikes(neuron)
    else
        SESSIONS[id][:neurons][I] = NeurophysToolbox.Neuron(neuron)
    end
    SESSIONS[id][:neuron_parameters_cache] = nothing
    return Dict{String, Any}()
end
HTTP.register!(ROUTER, "POST", "/api/neuroviz/v1/neuron/*/convert", convert_neuron)

function split_purkinje_cell(req::HTTP.Request)
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/split, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]
    if ! isa(neuron, NeurophysToolbox.PurkinjeCell)
        return "cannot split a non-purkinje cell"
    end

    # Convert the PurkinjeCell into a Neuron and (if necessary ComplexSpikes)
    split_neurons = Vector{AbstractNeuron}(undef, 0)
    if cs_spike_indices(neuron) != nothing && length(cs_spike_indices(neuron)) > 0
        cs_neuron = ComplexSpikes(cs_spike_indices(neuron), cs_spike_indices_channel(neuron), sampling_rate(neuron); filename=filename(neuron))
        if haskey(neuron, :filename)
            cs_neuron[:filename] = neuron[:filename]
        end
        push!(split_neurons, cs_neuron)
    end
    if spike_indices(neuron) != nothing && length(spike_indices(neuron)) > 0
        push!(split_neurons, Neuron(neuron)) # Regular conversion to normal neuron
    end

    # Delete the old neuron
    deleteat!(SESSIONS[id][:neurons], I)
    append!(SESSIONS[id][:neurons], split_neurons)

    d = Dict{String, Any}()
    d["num_neurons"] = length(split_neurons)
    d["neurons"] = Vector{Dict{String, Any}}(undef, 0)
    for neuron = split_neurons
        push!(d["neurons"], _neuron_to_dict(neuron, SESSIONS[id][:recording]))
    end
    return d
end
HTTP.register!(ROUTER, "POST", "/api/neuroviz/v1/neuron/*/split", split_purkinje_cell)

function get_spiketimes(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS[id], :recording) || SESSIONS[id][:recording] == nothing
        return "no valid recording"
    end
    recording = SESSIONS[id][:recording]
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/split, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]

    uri = HTTP.URIs.URI(req.target)
    x = HTTP.queryparams(uri)
    start_time = get(x, "start_time", -Inf)
    end_time = get(x, "end_time", Inf)
    type = get(x, "type", "nothing")

    try
        start_time = parse(Float32, start_time)
        end_time = parse(Float32, end_time)
    catch
        return "unable to parse start/end time"
    end

    if start_time >= end_time
        return "invalid start/end time"
    end

    spiketimes = spike_indices(neuron) * dt(neuron)
    select = (spiketimes .>= start_time) .& (spiketimes .<= end_time)
    cs_spiketimes = []
    cs_select = trues(0)
    if isa(neuron, NeurophysToolbox.PurkinjeCell)
        cs_spiketimes = cs_spike_indices(neuron) .* dt(neuron)
        cs_select = (cs_spiketimes .>= start_time) .& (cs_spiketimes .<= end_time)
    end
    return Dict{String, Any}("spiketimes" => spiketimes[select], "cs_spiketimes" => cs_spiketimes[cs_select], "start_time" => start_time, "end_time" => end_time)
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/neuron/*/spiketimes", get_spiketimes)

function get_clips(req::HTTP.Request)
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/acg, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]
    uri = HTTP.URIs.URI(req.target)
    x = HTTP.queryparams(uri)
    num_clips = get(x, "num", "50")
    try
        num_clips = parse(UInt64, num_clips)
    catch
        return "invalid number of clips specified"
    end
    channel = get(x, "channel", nothing)
    clips = nothing
    if channel == nothing
        clips = NeurophysToolbox.get_clips(neuron, SESSIONS[id][:recording])
    else
        try
            channel = parse(UInt64, num_clips)
        catch
            return "invalid channel"
        end
        if (channel) > length(SESSIONS[id][:recording])
            return "invalid channel"
        end
        clips = NeurophysToolbox.get_clips(neuron, SESSIONS[id][:recording], channel=channel)
    end

    return Dict("values" => channel, "dt" => dt(neuron))
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/neuron/*/clips", get_clips)

"""
    _compute_projection(session_id, task_id, channel, neuron, projection_type, [max_clips, neuron_2])

The optional argument neuron_2 uses a combination of the two units templates to create
the space to do the projection.
"""
function _compute_projection(session_id::UInt128, task_id::UInt128, channel::Channel, neuron::AbstractNeuron, projection_type::String; max_clips::Integer=5000, neuron_2::Union{Nothing, AbstractNeuron}=nothing)
    Y = [0, 0]
    model = nothing
    clips = convert.(Float32, NeurophysToolbox.get_clips(neuron, SESSIONS[session_id][:recording], channel=1:length(SESSIONS[session_id][:recording]))')
    clip_select = 1:size(clips, 2)
    if size(clips, 2) > max_clips
        clip_select = rand(1:size(clips, 2), max_clips)
    end

    secondary_clips = nothing
    if neuron_2 != nothing
        secondary_clips = convert.(Float32, NeurophysToolbox.get_clips(neuron_2, SESSIONS[session_id][:recording], channel=1:length(SESSIONS[session_id][:recording]))')
        secondary_select = 1:size(secondary_clips, 2)
        if size(secondary_clips, 2) > max_clips
            secondary_select = rand(1:size(secondary_clips, 2), max_clips)
        end   
        secondary_clips = secondary_clips[:, secondary_select]
    end
    if projection_type == "umap"
        if neuron_2 == nothing
            model = UMAP.UMAP_(clips[:, clip_select], 2, n_neighbors=10, min_dist=0.01)
        else
            model = UMAP.UMAP_(secondary_clips, 2, n_neighbors=10, min_dist=0.01)
        end
        Y = UMAP.transform(model, clips)'
    else
        # Project into a 2D space using PCA
        if neuron_2 == nothing
            model = MultivariateStats.fit(MultivariateStats.PCA, clips[:, clip_select]; maxoutdim=2)
        else
            model = MultivariateStats.fit(MultivariateStats.PCA, secondary_clips, maxoutdim=2)
        end
        Y = MultivariateStats.transform(model, clips)'
    end
    put!(channel, (task_id, projection_type == "umap" ? "umap-projection" : "pca-projection"))
    return Dict("x" => Y[:, 1], "y" => Y[:, 2])
end

function get_template_projection(req::HTTP.Request)
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/template-projection, get neuron #
    catch
        return "invalid neuron hash/unparseable"
    end
    # Find the neuron
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron hash/not found"
    end
    neuron = SESSIONS[id][:neurons][I]
    uri = HTTP.URIs.URI(req.target)
    x = HTTP.queryparams(uri)
    projection_type = get(x, "type", "pca")
    secondary_neuron_id = get(x, "second_unit_id", nothing)
    try
        secondary_neuron_id = parse(UInt64, secondary_neuron_id)
    catch
        secondary_neuron_id = nothing
    end
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== secondary_neuron_id)
    neuron_2 = nothing
    if I != nothing
        neuron_2 = SESSIONS[id][:neurons][I]
    end
    task_id, _ = run_task(id, _compute_projection, neuron, projection_type, label=projection_type, neuron_2=neuron_2)
    return Dict{String, Any}("status" => "processing", "task_id" => string(task_id))
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/neuron/*/template-projection", get_template_projection)

function modify_neuron(req::HTTP.Request)
    body = IOBuffer(HTTP.payload(req))
    # Expects a list of spike indices to modify
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    neuron_id = nothing
    try
        neuron_id = parse(UInt64, paths[5]) # /api/neuroviz/v1/neuron/#/modify, get neuron #
    catch e
        return string(e, "invalid neuron hash/unparseable")
    end
    I = findfirst([hash(x) for x in SESSIONS[id][:neurons]] .== neuron_id)
    if I == nothing
        return "invalid neuron index"
    end
    neuron = SESSIONS[id][:neurons][I]

    x = nothing
    try
        x = JSON2.read(body)
    catch ex
        return string(ex)
    end
    if ! haskey(x, :spikes)
        return "invalid data - expected list of spike index values"
    end
    indices = nothing
    try
        println(x[:spikes])
        indices = convert.(Int64, x[:spikes]) .+ 1 # Fix zero index java script
    catch e
        return string(e, "invalid list of spike index values/unparseable")
    end
    # Sort and get unique values
    indices = unique(sort(indices))

    # Ensure the indices are valid
    if indices[1] <= 0 || indices[end] > length(spike_indices(neuron))
        return "invalid indices - exceeds bounds of spikes"
    end

    command_type = get(x, :type, "delete")

    if command_type == "delete"
        deleteat!(neuron.spike_indices, indices)
        deleteat!(neuron.spike_indices_channel, indices)
        return Dict{String, Any}("neurons" => [_neuron_to_dict(neuron, SESSIONS[id][:recording])])
    elseif command_type == "split"
        new_neuron = Neuron(spike_indices(neuron)[indices], spike_indices_channel(neuron)[indices], sampling_rate(neuron), filename=filename(neuron))
        if haskey(neuron, :filename)
            new_neuron[:filename] = neuron[:filename]
        end
        push!(SESSIONS[id][:neurons], new_neuron)
        deleteat!(neuron.spike_indices, indices)
        deleteat!(neuron.spike_indices_channel, indices)
        return Dict{String, Any}("neurons" => [_neuron_to_dict(neuron, SESSIONS[id][:recording]), _neuron_to_dict(new_neuron, SESSIONS[id][:recording])])
    else
        return string("invalid command for passed indices")
    end
    SESSIONS[id][:neuron_parameters_cache] = nothing
end
HTTP.register!(ROUTER, "POST", "/api/neuroviz/v1/neuron/*/modify", modify_neuron)
