
function get_voltage_across_channels(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS[id], :recording) || SESSIONS[id][:recording] == nothing
        return "no valid recording"
    end

    recording = SESSIONS[id][:recording]

    uri = HTTP.URIs.URI(req.target)
    x = HTTP.queryparams(uri)
    start_time = get(x, "start_time", "0")
    end_time = get(x, "end_time", "1.0")

    # TODO: get channels
    channels = 1:length(recording)

    try
        start_time = parse(Float32, start_time)
        end_time = parse(Float32, end_time)
    catch
        return "unable to parse start/end time"
    end

    if start_time >= end_time
        return "invalid start/end time"
    end
    dts = Vector{Real}(undef, 0)
    voltages = Vector{Vector{Real}}(undef, 0)
    limits = Vector{Vector{Real}}(undef, 0)
    for channel = channels
        push!(dts, dt(recording[channel][SpikeTimeseries]))
        start_index = max(Integer(round(start_time / dt(recording[channel][SpikeTimeseries]))), 1)
        end_index = min(length(recording[channel][SpikeTimeseries]), Integer(round(end_time / dt(recording[channel][SpikeTimeseries]))))
        push!(limits, [start_index, end_index] .* dt(recording[channel][SpikeTimeseries]))
        push!(voltages, recording[channel][SpikeTimeseries][start_index:end_index])
    end

    return Dict{String, Any}("voltage" => voltages, "dt" => dts, "limits" => limits, "channels" => channels, "start_time" => start_time, "end_time" => end_time)
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/voltage", get_voltage_across_channels)
