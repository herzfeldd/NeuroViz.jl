"""
    get_session_id(req)

Returns the session ID or nothing based on the request header
"""
function get_session_id(req::HTTP.Request)
    if HTTP.hasheader(req, "SessionId")
         return parse(keytype(SESSIONS), HTTP.header(req, "SessionId"))
     end
    return nothing
end


"""
    CORSResponse(response)

Allow Cross-Origin Resource Sharing
"""
function CORSResponse(response::HTTP.Response, content_type="application/json, application/octet-stream")
    push!(response.headers, Pair("Access-Control-Allow-Origin", "*"))
    push!(response.headers, Pair("Access-Control-Allow-Methods", "*"))
    push!(response.headers, Pair("Access-Control-Allow-Headers", "*"))
    push!(response.headers, Pair("Access-Control-Max-Age", "3600"))
    push!(response.headers, Pair("Access-Control-Expose-Headers", "*"))
    push!(response.headers, Pair("Content-Type", content_type))
    #push!(response.headers, Pair("Content-Type", "application/json, multipart/form-data"))
    return response
end
