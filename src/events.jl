mutable struct BackgroundTask
    task::Task
    task_id::UInt128
    session_id::UInt128
    label::Union{Nothing, AbstractString}
    retries::Integer
end
function BackgroundTask(task::Task, task_id::UInt128, session_id::UInt128, label::Union{Nothing, AbstractString})
    return BackgroundTask(task, task_id, session_id, label, 0)
end
Base.istaskdone(x::BackgroundTask) = Base.istaskdone(x.task)
Base.istaskfailed(x::BackgroundTask) = Base.istaskfailed(x.task)
Base.istaskstarted(x::BackgroundTask) = Base.istaskstarted(x.task)

function events(stream::HTTP.Stream)
    paths = HTTP.URIs.splitpath(stream.message.target)
    id = nothing
    try
        id = parse(UInt128, paths[5]) # /api/neuroviz/v1/events/#, get session #
    catch
        return nothing
    end

    if id == nothing || ! haskey(SESSIONS, id)
        return nothing
    end

    HTTP.setheader(stream, "Access-Control-Allow-Origin" => "*")
    HTTP.setheader(stream, "Access-Control-Allow-Methods" => "GET, OPTIONS")
    HTTP.setheader(stream, "Content-Type" => "text/event-stream")

    if HTTP.method(stream.message) == "OPTIONS"
        return nothing
    end

    @async while true
        # Wake up every 5s and add a ping event
        put!(SESSIONS[id][:session].task_channel, (0, "ping"))
        sleep(1) # Wake up once a second to check for connected clients
    end

    HTTP.setheader(stream, "Content-Type" => "text/event-stream")
    HTTP.setheader(stream, "Cache-Control" => "no-cache")
    while true
        while isready(SESSIONS[id][:session].task_channel)
            message = take!(SESSIONS[id][:session].task_channel)
            write(stream, "event: $(string(message[1]))\n")
            write(stream, "id: $(string(message[1]))\n")
            write(stream, "data: $(string(message[2]))\n\n")
        end
        # Check if there are any tasks that are finished and resend those events
        # In case the receiver missed them.

        for (key, background_task) = SESSIONS[id][:session].background_tasks
            if istaskdone(background_task)
                write(stream, "event: $(string(key))\n")
                write(stream, "id: $(string(key))\n")
                write(stream, "data: resend\n\n")
                background_task.retries = background_task.retries + 1
            end
        end

        # Check to see if any tasks have failed, if so we can delete them
        for key = keys(SESSIONS[id][:session].background_tasks)
            if SESSIONS[id][:session].background_tasks[key].retries > 10
                write(stream, "event: $(string(key))\n")
                write(stream, "id: $(string(key))\n")
                write(stream, "data: cancelled\n\n")

                println("Deleting task with id $key after ten failed retries.")
                delete!(SESSIONS[id][:session].background_tasks, key)
            elseif istaskfailed(SESSIONS[id][:session].background_tasks[key])
                e = "failed"
                try
                    fetch(SESSIONS[id][:session].background_tasks[key].task)
                catch e
                    @error(exception = (e, catch_backtrace()))
                end
                write(stream, "event: $(string(key))\n")
                write(stream, "id: $(string(key))\n")
                write(stream, "data: $e\n\n")
                println("Deleting failed task with id $key.")
                delete!(SESSIONS[id][:session].background_tasks, key)
            end
        end

        wait(SESSIONS[id][:session].task_channel)
    end
    return nothing
end

HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/events/*", events)

function run_task(id::UInt128, f::Function, args...; label::Union{Nothing, AbstractString}=nothing, kwargs...)
    # Generate a new task id
    task_id::UInt128 = 0
    while true
        task_id = rand(UInt128)
        (haskey(SESSIONS[id][:session].background_tasks, task_id)) || break
    end
    # Ensure there are no other tasks with our label
    if label != nothing
        for (key, background_task) = SESSIONS[id][:session].background_tasks
            if background_task.label == label && istaskstarted(background_task) && ! istaskdone(background_task)
                println("Pre-empting task with id $(background_task.task_id)")
                _cancel_task(background_task)
            end
        end
    end

    # Schedule our task
    t = Threads.@spawn f(id, task_id, SESSIONS[id][:session].task_channel, args...; kwargs...)
    current_background_task = BackgroundTask(t, task_id, id, label)
    # Place in our task queue
    SESSIONS[id][:session].background_tasks[task_id] = current_background_task
    return task_id, current_background_task
end

function get_task(req::HTTP.Request)
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    task_id = nothing
    try
        task_id = parse(UInt128, paths[5]) # /api/neuroviz/v1/task/#
    catch
        return "unable to parse task id"
    end
    if ! haskey(SESSIONS[id][:session].background_tasks, task_id)
        return "invalid task id"
    end
    if ! istaskstarted(SESSIONS[id][:session].background_tasks[task_id])
        return Dict{String, Any}("status" => "waiting")
    end
    # Ths is strange...if we get here, presumably we are done because we
    # have posted via the event channel that we are done. But it is possible
    # the task hasn't been reaped yet. So not sure what to do here besides
    # wait for the fetch
    # if ! istaskdone(SESSIONS[id][:session].background_tasks[task_id])
    #     return Dict{String, Any}("status" => "processing")
    # end
    results = fetch(SESSIONS[id][:session].background_tasks[task_id].task)
    if istaskfailed(SESSIONS[id][:session].background_tasks[task_id])
        results = Dict{String, Any}("status" => "failed")
    end
    # If we were able to fetch the results, we can delete the task
    delete!(SESSIONS[id][:session].background_tasks, task_id)
    return results # Should be a dict or string
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/task/*", get_task)

function _cancel_task(task::BackgroundTask)
    # Force cancellation via interrupt. Ideally, we would send a signal via
    # a channel made for that specific task, but this won't let us cancel in the
    # middle of an operation. This should be fine provided this task isn't
    # accessing any global structures.
    try
        schedule(task.task, InterruptException(), error=true)
    catch
    end
end

function delete_task(req::HTTP.Request)
    id = get_session_id(req)
    paths = HTTP.URIs.splitpath(req.target)
    task_id = nothing
    try
        task_id = parse(UInt128, paths[5]) # /api/neuroviz/v1/task/#
    catch
        return "unable to parse task id"
    end
    if ! haskey(SESSIONS[id][:session].background_tasks, task_id)
        return "invalid task id"
    end
    if (! istaskdone(SESSIONS[id][:session].background_tasks[task_id])) && istaskstarted(SESSIONS[id][:session].background_tasks[task_id])
        _cancel_task(SESSIONS[id][:session].background_tasks[task_id])
    end

    delete!(SESSIONS[id][:session].background_tasks, task_id)
    return Dict()
end
HTTP.register!(ROUTER, "DELETE", "/api/neuroviz/v1/task/*", delete_task)
