function create_session(req::HTTP.Request)
    session_id = rand(UInt128)
    SESSIONS[session_id] = Dict{Symbol, Any}()
    SESSIONS[session_id][:session] = Session() # Add an empty session to our list of sessions
    return Dict{String, Any}("SessionId" => string(session_id))
end
HTTP.register!(ROUTER, "POST", "/api/neuroviz/v1/session", create_session)

function delete_session(session_id::UInt128)
    _delete_session_files(SESSIONS[session_id])
    # Cancel all running tasks
    if haskey(SESSIONS[session_id], :session)
        for (task_id, task) = SESSIONS[session_id][:session].background_tasks
            if ! istaskdone(task)
                _cancel_task(task)
            end
        end
    end
    delete!(SESSIONS, session_id)
end

function delete_session(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS, id)
        return "invalid session id, not found"
    end
    delete_session(id)
    return Dict()
end
HTTP.register!(ROUTER, "DELETE", "/api/neuroviz/v1/session", delete_session)


function get_session(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS, id)
        return "invalid session id, not found"
    end
    SESSIONS[id][:last_check_in] = Dates.now()
    return Dict()
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/session", get_session)

function _process_pl2_file(session_id::UInt128, task_id::UInt128, channel::Channel, filename::AbstractString)
    recording = PL2Recording(filename)
    if any([haskey(ch, SpikeTimeseries) for ch in recording] .== false)
        NeurophysToolbox.preprocess!(recording)
    end
    put!(channel, (task_id, "recording-file"))
    # Set the recording file and return our response
    d = Dict{String, Any}()
    d["status"] = "ready"
    d["num_channels"] = length(recording)
    if d["num_channels"] > 0
        d["sampling_rate"] = [sampling_rate(ch[SpikeTimeseries]) for ch in recording]
        d["num_samples"] = [length(ch[SpikeTimeseries]) for ch in recording]
    end
    SESSIONS[session_id][:recording] = recording

    return d
end

function _process_flat_file(session_id::UInt128, task_id::UInt128, channel::Channel, filename::AbstractString, num_channels::Integer, sampling_rate::Real, interleaved=false, mmap::Bool=false)
    recording = FlatRecording(filename, Int64(num_channels), sampling_rate, interleaved=interleaved, mmap=mmap)
    if any([haskey(ch, SpikeTimeseries) for ch in recording] .== false)
        NeurophysToolbox.preprocess!(recording)
    end
    put!(channel, (task_id, "recording-file"))
    # Set the recording file and return our response
    d = Dict{String, Any}()
    d["status"] = "ready"
    d["num_channels"] = length(recording)
    if d["num_channels"] > 0
        d["sampling_rate"] = [NeurophysToolbox.sampling_rate(ch[SpikeTimeseries]) for ch in recording]
        d["num_samples"] = [length(ch[SpikeTimeseries]) for ch in recording]
    end
    SESSIONS[session_id][:recording] = recording
    return d
end

function _delete_session_files(session)
    if ! haskey(session, :session)
        return
    end
    if session[:session].neuron_filename != nothing
        try
            Base.Filesystem.rm(session[:session].neuron_filename, force=true)
        catch

        end
    end
    if session[:session].recording_filename != nothing && haskey(session, :recording_is_temp) && session[:recording_is_temp]
        try
            Base.Filesystem.rm(session[:session].recording_filename, force=true)
        catch

        end
    end
end

function set_server_recording_file!(req::HTTP.Request)
    body = IOBuffer(HTTP.payload(req))
    id = get_session_id(req)
    uri = HTTP.URIs.URI(req.target)
    x = HTTP.queryparams(uri)
    type = get(x, "type", "pl2")
    num_channels = nothing
    sampling_rate = nothing
    interleaved = nothing
    mmap = nothing
    if type != "pl2" && type != "flat"
        return "invalid recording type/expected pl2 or flat"
    end
    if type == "flat"
        if ! haskey(x, "sampling_rate") || ! haskey(x, "num_channels")
            return "flat recordings require sampling_rate and num_channels parameters"
        end
        num_channels = x["num_channels"]
        sampling_rate = x["sampling_rate"]
        interleaved = get(x, "interleaved", "false")
        mmap = get(x, "mmap", "true")
    end

    x = nothing
    try
        x = JSON2.read(body)
    catch ex
        return return string(ex)
    end
    if ! haskey(x, :path)
        return "path not specified"
    end
    recording_filename = x[:path]

    if ! Base.Filesystem.isfile(recording_filename)
        return "invalid filename"
    end
    # Check to make sure the type is appeopriate
    _, ext = Base.Filesystem.splitext(recording_filename)
    if (type == "pl2" && ext != ".pl2") || (type == "flat" && (ext != ".bin") && (ext != ".dat"))
        return "invalid extension"
    end

    # Get the complete path and make sure we are allowed to look at this file
    recording_filename = Base.Filesystem.realpath(recording_filename)
    allowed = false
    for allowed_path = ALLOWED_LOCAL_PATHS
        if occursin(allowed_path, recording_filename)
            allowed = true
        end
    end
    if allowed == false
        return "permission denied for path $recording_filename"
    end
    _delete_session_files(SESSIONS[id])
    SESSIONS[id][:session].recording_filename = recording_filename
    SESSIONS[id][:recording_is_temp] = false

    if type == "pl2"
        task_id, _ = run_task(id, _process_pl2_file, SESSIONS[id][:session].recording_filename)
    else
        try
            num_channels = parse(UInt64, num_channels) # /api/neuroviz/v1/neuron/#/acg, get neuron #
            sampling_rate = parse(Float32, sampling_rate) # /api/neuroviz/v1/neuron/#/acg, get neuron #
            interleaved = parse(Bool, interleaved)
            mmap = parse(Bool, mmap)
        catch
            return "failed to parse num_channels, sampling_rate, interleaved, or mmap"
        end
        task_id, _ = run_task(id, _process_flat_file, SESSIONS[id][:session].recording_filename, num_channels, sampling_rate, interleaved, mmap)
    end

    return Dict{String, Any}("status" => "processing", "task_id" => string(task_id))
end
HTTP.register!(ROUTER, "POST", "/api/neuroviz/v1/session/server_recording_file", set_server_recording_file!)

function set_recording_file!(req::HTTP.Request,)
    body = IOBuffer(HTTP.payload(req))
    id = get_session_id(req)

    uri = HTTP.URIs.URI(req.target)
    x = HTTP.queryparams(uri)
    type = get(x, "type", "pl2")
    num_channels = nothing
    sampling_rate = nothing
    mmap = nothing
    interleaved = nothing
    if type != "pl2" && type != "flat"
        return "invalid recording type/expected pl2 or flat"
    end
    if type == "flat"
        if ! haskey(x, "sampling_rate") || ! haskey(x, "num_channels")
            return "flat recordings require sampling_rate and num_channels parameters"
        end
        num_channels = x["num_channels"]
        sampling_rate = x["sampling_rate"]
        interleaved = get(x, "interleaved", "false")
        mmap = get(x, "mmap", "true")
    end
    _delete_session_files(SESSIONS[id])
    SESSIONS[id][:session].recording_filename = Base.Filesystem.tempname()
    fp = open(SESSIONS[id][:session].recording_filename, "w") # TODO: Open with JL_O_EXCL
    write(fp, body)
    close(fp)
    SESSIONS[id][:recording_is_temp] = true

    # Remove any old recordings
    if haskey(SESSIONS[id], :recording)
        delete!(SESSIONS[id], :recording)
    end

    if type == "pl2"
        task_id, _ = run_task(id, _process_pl2_file, SESSIONS[id][:session].recording_filename)
    else
        try
            num_channels = parse(UInt64, num_channels) # /api/neuroviz/v1/neuron/#/acg, get neuron #
            sampling_rate = parse(Float32, sampling_rate) # /api/neuroviz/v1/neuron/#/acg, get neuron #
            interleaved = parse(Bool, interleaved)
            mmap = parse(Bool, mmap)
        catch
            return "failed to parse num_channels, sampling_rate, interleaved, or mmap"
        end
        task_id, _ = run_task(id, _process_flat_file, SESSIONS[id][:session].recording_filename, num_channels, sampling_rate, interleaved, mmap)
    end

    return Dict{String, Any}("status" => "processing", "task_id" => string(task_id))
end
HTTP.register!(ROUTER, "POST", "/api/neuroviz/v1/session/recording_file", set_recording_file!)

function get_recording_file(req::HTTP.Request)
    id = get_session_id(req)
    if SESSIONS[id][:session].recording_filename == nothing
        return "invalid request/unopen"
    end
    d = Dict{String, Any}()
    d["status"] = "ready"
    recording = SESSIONS[id][:recording]
    d["num_channels"] = length(recording)
    if d["num_channels"] > 0
        d["sampling_rate"] = [sampling_rate(ch[SpikeTimeseries]) for ch in recording]
        d["num_samples"] = [length(ch[SpikeTimeseries]) for ch in recording]
    end
    return d
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/session/recording_file", get_recording_file)

function set_neuron_file!(req::HTTP.Request)
    body = IOBuffer(HTTP.payload(req))
    id = get_session_id(req)
    form = nothing
    try
        form = HTTP.parse_multipart_form(req)
    catch ex
        return string(ex)
    end
    if (length(form) != 1)
        return "unexpected number of files found/expect 1, got $(length(form))"
    end
    if form[1].filename == nothing || ((! occursin(r"\.jld2", form[1].filename)) && (! occursin(r"\.mat", form[1].filename)) && (! occursin(r"\.pkl", form[1].filename)))
        return "invalid filename specified/jld2 or mat extension required"
    end
    if SESSIONS[id][:session].neuron_filename != nothing
        try
            Base.Filesystem.rm(SESSIONS[id][:session].neuron_filename, force=true)
        catch

        end
    end

    _, ext = Base.Filesystem.splitext(form[1].filename)
    SESSIONS[id][:session].neuron_filename = string(Base.Filesystem.tempname(), ext)
    SESSIONS[id][:neuron_file_is_temp] = true
    fp = open(SESSIONS[id][:session].neuron_filename, "w") # TODO: Open with JL_O_EXCL
    write(fp, form[1].data)
    close(fp)

    # Read the newly created file
    neurons = nothing
    try
        neurons = load_neurons(SESSIONS[id][:session].neuron_filename)
        neurons = convert(Vector{AbstractNeuron}, neurons);
    catch
        return "invalid neuron file/unreadable"
    end

    for neuron = neurons
        neuron[:filename] = form[1].filename
        #NeurophysToolbox.align_clips!(neuron, SESSIONS[id][:recording])
    end

    uri = HTTP.URIs.URI(req.target)
    x = HTTP.queryparams(uri)
    append = get(x, "append", "false")
    try
        append = parse(Bool, append)
    catch
        append = false
    end

    if haskey(SESSIONS[id], :neuron_parameters_cache)
        SESSIONS[id][:neuron_parameters_cache] = nothing
    end

    if append == false || ! haskey(SESSIONS[id], :neurons) || length(SESSIONS[id][:neurons]) == 0
        SESSIONS[id][:neurons] = neurons
    else
        append!(SESSIONS[id][:neurons], neurons);
    end

    return Dict{String, Any}()
end
HTTP.register!(ROUTER, "POST", "/api/neuroviz/v1/session/neuron_file", set_neuron_file!)


function get_neuron_file!(req::HTTP.Request)
    id = get_session_id(req)
    if ! haskey(SESSIONS[id], :neurons) || SESSIONS[id][:neurons] == nothing
        return "no neurons/invalid structure"
    end
    uri = HTTP.URIs.URI(req.target)
    x = HTTP.queryparams(uri)
    file_type = get(x, "type", "jld2")

    tmp_file = string(Base.Filesystem.tempname(), ".", file_type)
    save_neurons(tmp_file, SESSIONS[id][:neurons])

    # We need to return the file with the header attachment
    contents = nothing
    open(tmp_file) do file
        contents = read(file)
    end
    try
        Base.Filesystem.rm(tmp_file)
    catch e
        println(e)
    end
    response = CORSResponse(HTTP.Response(200,contents), "application/octet-stream")
    push!(response.headers, Pair("Content-Disposition", string("attachment; filename=\"", string("todo.", file_type), "\"")))
    push!(response.headers, Pair("Content-Length", string(length(contents))));
    return response
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/session/neuron_file", get_neuron_file!)

function get_session_notes(req::HTTP.Request)
    id = get_session_id(req)

    notes = ""
    if haskey(SESSIONS[id], :recording) && haskey(SESSIONS[id][:recording], :notes)
        notes = SESSIONS[id][:recording][:notes]
    end
    if haskey(SESSIONS[id], :recording) && haskey(SESSIONS[id][:recording], :creator_comments)
        notes = SESSIONS[id][:recording][:creator_comments]
    end

    # If there are no notes, search for PL2 file in the same directory and
    # grab those notes from the header...
    if length(notes) == 0
        _, original_extension = Base.Filesystem.splitext(SESSIONS[id][:session].recording_filename)
        if original_extension != ".pl2"
            pl2_file = replace(SESSIONS[id][:session].recording_filename, original_extension => ".pl2")
            if Base.Filesystem.isfile(pl2_file)
                info = NeurophysToolbox.PL2.load_file_information(pl2_file)
                if haskey(info, :creator_comment)
                    notes = info[:creator_comment]
                end
            end
        end
    end

    return Dict{String, Any}("notes" => notes)
end
HTTP.register!(ROUTER, "GET", "/api/neuroviz/v1/session/notes", get_session_notes)
